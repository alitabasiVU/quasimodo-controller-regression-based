function Trial1 = importfile(filename, dataLines)
%IMPORTFILE Import data from a text file
%  TRIAL1 = IMPORTFILE(FILENAME) reads data from text file FILENAME for
%  the default selection.  Returns the data as a table.
%
%  TRIAL1 = IMPORTFILE(FILE, DATALINES) reads data for the specified row
%  interval(s) of text file FILENAME. Specify DATALINES as a positive
%  scalar integer or a N-by-2 array of positive scalar integers for
%  dis-contiguous row intervals.
%
%  Example:
%  Trial1 = importfile("/Users/ali/Documents/PhD/TNO_ROBOMATE/RegressionModelImplementation-TNOExo/On-Site scripts/RegressionModelCalibration/CalibrationFiles/Trial1.txt", [2, Inf]);
%
%  See also READTABLE.
%
% Auto-generated by MATLAB on 25-Apr-2022 18:00:03

%% Input handling

% If dataLines is not specified, define defaults
if nargin < 2
    dataLines = [2, Inf];
end

%% Setup the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 15);

% Specify range and delimiter
opts.DataLines = dataLines;
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["timesample", "gQTr_raww", "gQTr_rawx", "gQTr_rawy", "gQTr_rawz", "gQPe_raww", "gQPe_rawx", "gQPe_rawy", "gQPe_rawz", "TrunkInclination", "LumbarAngle", "ActiveMoment", "reference", "MoveMeanEMG","EMGAverage"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

% Import the data
Trial1 = readtable(filename, opts);

end