%% Read Data From Full Flexion Trial

[FlexionRelaxationAngle,I] = max(CalibrationDataSet.trial(FullFlexionTrialNumber).LumbarAngle);
FlexionRelaxationMoment = CalibrationDataSet.trial(FullFlexionTrialNumber).ML5S1(I,2);
%%%
angle = [deg2rad([0 10 15 20])  FlexionRelaxationAngle];
PassiveMomentParams = polyfit(angle, [0 0 0 0  FlexionRelaxationMoment],4);
%figure, plot(-10:1:FlexionRelaxationAngle*1.2,polyval(PassiveMomentParams,-10:1:FlexionRelaxationAngle*1.2));
save('../../src/PassiveParams.mat','PassiveMomentParams');
