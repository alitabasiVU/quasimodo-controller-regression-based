%%%%%%%%% Inverse Dynamics

%%%%%%%%%%%%%%% Constant values based on  Leva 1996 %%%%%%%%%%%%%%%%%%%
SegmentMass = ...        % % of body mass, From de Leva 1996 adjustments to Zatsiorsky J. Biomechanics 29 
    [  6.68 6.94         % Head
       42.57 43.46       % Trunk
       2.55 2.71         % Upper Arm
       1.38 1.62         % Forearm
       0.56 0.61         % Hand
    ];
SegmentMass = SegmentMass/100;

SegmentCMPosition =  ...    % % of segment length, From de Leva 1996 adjustments to Zatsiorsky J. Biomechanics 29 
    [ 1-.4841 1-.5002       % Head , ratios are from Vert to Cerv, 1- is to calculate CM position from Cerv to Vert
     .46 .42                % Trunk, ratio is the distance from Iliospinale (~L5S1 Level) to Cervical
     .5754 .5772            % Upper Arm, From Sholder joint
     .4559 .4574            % Forarm , From Elbow joint center
     .7474 .79              % Hand, from wrist joint center
    ];
g = [0 0 -9.81] ;


for trialnum = CalibrationTrials 
    UpperArmAngle = CalibrationDataSet.trial(trialnum).UpperArmAngle;
    ForearmAngle = CalibrationDataSet.trial(trialnum).ForearmAngle ;
    BoxMass = CalibrationDataSet.trial(trialnum).BoxMass;
    lengthtrial = length(CalibrationDataSet.trial(trialnum).inclination);
    d_L5S1_HeadCOM = [ (SegmentCMPosition(1,Gender)*SegmentLength(1)+SegmentLength(2))*sin(CalibrationDataSet.trial(trialnum).inclination),...    % distance in x direction
                        zeros(lengthtrial,1),...                                                                                                   % distance in y direction
                       (SegmentCMPosition(1,Gender)*SegmentLength(1)+SegmentLength(2))*cos(CalibrationDataSet.trial(trialnum).inclination),...    % distance in z direction
                        ];
                    
    d_L5S1_TrunkCOM = [ SegmentCMPosition(2,Gender)*SegmentLength(2)*sin(CalibrationDataSet.trial(trialnum).inclination),...    % distance in x direction
                        zeros(lengthtrial,1),...                                                                                 % distance in y direction
                        SegmentCMPosition(2,Gender)*SegmentLength(2)*cos(CalibrationDataSet.trial(trialnum).inclination),...    % distance in z direction
                        ];
                    
    d_L5S1_UpperArmCOM = [ (SegmentLength(2))*sin(CalibrationDataSet.trial(trialnum).inclination)+cos(UpperArmAngle)*SegmentCMPosition(3,Gender)*SegmentLength(3),...     % distance in x direction
                           zeros(lengthtrial,1),...                                                                                                                         % distance in y direction
                           (SegmentLength(2))*cos(CalibrationDataSet.trial(trialnum).inclination)+sin(UpperArmAngle)*SegmentCMPosition(3,Gender)*SegmentLength(3),...    % distance in z direction
                         ];
                     
    d_L5S1_ForearmCOM = [ (SegmentLength(2))*sin(CalibrationDataSet.trial(trialnum).inclination)+cos(UpperArmAngle)*SegmentLength(3)+cos(ForearmAngle)*SegmentCMPosition(4,Gender)*SegmentLength(4),...     % distance in x direction
                           zeros(lengthtrial,1),...                                                                                                                         % distance in y direction
                           (SegmentLength(2))*cos(CalibrationDataSet.trial(trialnum).inclination)+sin(UpperArmAngle)*SegmentLength(3)+sin(ForearmAngle)*SegmentCMPosition(4,Gender)*SegmentLength(4),...    % distance in z direction
                         ];
    d_L5S1_Hand = [(SegmentLength(2))*sin(CalibrationDataSet.trial(trialnum).inclination)+cos(UpperArmAngle)*SegmentLength(3)+cos(ForearmAngle)*SegmentLength(4)+cos(ForearmAngle)*SegmentCMPosition(5,Gender)*SegmentLength(5),...     % distance in x direction
                   zeros(lengthtrial,1),...                                                                                                                         % distance in y direction
                   (SegmentLength(2))*cos(CalibrationDataSet.trial(trialnum).inclination)+sin(UpperArmAngle)*SegmentLength(3)+sin(ForearmAngle)*SegmentLength(4)+sin(ForearmAngle)*SegmentCMPosition(5,Gender)*SegmentLength(5),...    % distance in z direction
                  ];
    ML5S1 = [];          
    for tnum = 1:lengthtrial
        ML5S1(tnum,:) = cross(d_L5S1_HeadCOM(tnum,:),BodyMass*SegmentMass(1,Gender)*g) +...
                        cross(d_L5S1_TrunkCOM(tnum,:),BodyMass*SegmentMass(2,Gender)*g) +...
                        2*cross(d_L5S1_UpperArmCOM(tnum,:),BodyMass*SegmentMass(3,Gender)*g) +...
                        2*cross(d_L5S1_ForearmCOM(tnum,:),BodyMass*SegmentMass(4,Gender)*g) +...
                        2*cross(d_L5S1_Hand(tnum,:),BodyMass*SegmentMass(5,Gender)*g)+...
                        cross(d_L5S1_Hand(tnum,:),BoxMass*g);
    end
    CalibrationDataSet.trial(trialnum).d_L5S1_HeadCOM = d_L5S1_HeadCOM;
    CalibrationDataSet.trial(trialnum).d_L5S1_TrunkCOM = d_L5S1_TrunkCOM;
    CalibrationDataSet.trial(trialnum).d_L5S1_UpperArmCOM = d_L5S1_UpperArmCOM;
    CalibrationDataSet.trial(trialnum).d_L5S1_ForearmCOM = d_L5S1_ForearmCOM;
    CalibrationDataSet.trial(trialnum).d_L5S1_Hand = d_L5S1_Hand;
    CalibrationDataSet.trial(trialnum).UpperArmAngle = UpperArmAngle;
    CalibrationDataSet.trial(trialnum).ForearmAngle = ForearmAngle;
    CalibrationDataSet.trial(trialnum).ML5S1 = ML5S1;
end


