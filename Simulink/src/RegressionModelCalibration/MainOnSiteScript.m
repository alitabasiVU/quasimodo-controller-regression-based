clc
clear all
close all

%% Subject Info

SubjectNumber = 1; 
BodyMass = 66;
BodyLength = 1.75;
Gender = 1;                 % 1 female, 2 male

SegmentLength = ...      % in mm, Should be meaasured during the preperation  
    [  260               % Head,Cerv to Vertex
       460             % Trunk,Iliospinale (~L5S1 Level) Joint to Cervical
       290             % Upper Arm,Shoulder to Elbow joint center
       240             % Forearm,Elbow to wrist joint center
       180               % Hand,Wrist to 3rd Metacarpale
    ];

SegmentLength = SegmentLength/1000; % mm to m;

%% Read calibration data

CalibrationTrials = [2 9];


for CalibratoinTrialNum = CalibrationTrials
    calibdatatable = ReadLogFileFunction(['CalibrationFiles/Trial' num2str(CalibratoinTrialNum) '.txt']); 
    CalibrationDataSet.trial(CalibratoinTrialNum).inclination = calibdatatable.TrunkInclination;
    CalibrationDataSet.trial(CalibratoinTrialNum).LumbarAngle = calibdatatable.LumbarAngle;
%     CalibrationDataSet.trial(CalibratoinTrialNum).AverageMyoArmband = calibdatatable.MoveMeanEMG;
    clear calibdatatable
end

% HorizontalArmNoBox = [1];
% CalibrationDataSet.trial(HorizontalArmNoBox).UpperArmAngle = 0;
% CalibrationDataSet.trial(HorizontalArmNoBox).ForearmAngle = 0;
% CalibrationDataSet.trial(HorizontalArmNoBox).BoxMass = 0;

VerticalArmNoBox = [2];
CalibrationDataSet.trial(VerticalArmNoBox).UpperArmAngle = deg2rad(-90);
CalibrationDataSet.trial(VerticalArmNoBox).ForearmAngle = deg2rad(-90);
CalibrationDataSet.trial(VerticalArmNoBox).BoxMass = 0;

% Elbow90NoBox = [3];
% CalibrationDataSet.trial(Elbow90NoBox).UpperArmAngle = -90;
% CalibrationDataSet.trial(Elbow90NoBox).ForearmAngle = 0;
% CalibrationDataSet.trial(Elbow90NoBox).BoxMass = 0;
% 
% HorizontalArm5KGBox = [4];
% CalibrationDataSet.trial(HorizontalArm5KGBox).UpperArmAngle = 0;
% CalibrationDataSet.trial(HorizontalArm5KGBox).ForearmAngle = 0;
% CalibrationDataSet.trial(HorizontalArm5KGBox).BoxMass = 5;
% 
% VerticalArm5KGBox = [5];
% CalibrationDataSet.trial(VerticalArm5KGBox).UpperArmAngle = -90;
% CalibrationDataSet.trial(VerticalArm5KGBox).ForearmAngle = -90;
% CalibrationDataSet.trial(VerticalArm5KGBox).BoxMass = 5;
% 
% Elbow905KGBox = [6];
% CalibrationDataSet.trial(Elbow905KGBox).UpperArmAngle = -90;
% CalibrationDataSet.trial(Elbow905KGBox).ForearmAngle = 0;
% CalibrationDataSet.trial(Elbow905KGBox).BoxMass = 5;

% HorizontalArm15KGBox = [7];
% CalibrationDataSet.trial(HorizontalArm15KGBox).UpperArmAngle = 0;
% CalibrationDataSet.trial(HorizontalArm15KGBox).ForearmAngle = 0;
% CalibrationDataSet.trial(HorizontalArm15KGBox).BoxMass = 10;

% VerticalArm15KGBox = [7];
% CalibrationDataSet.trial(VerticalArm15KGBox).UpperArmAngle = -90;
% CalibrationDataSet.trial(VerticalArm15KGBox).ForearmAngle = -90;
% CalibrationDataSet.trial(VerticalArm15KGBox).BoxMass = 15;
% 
% Elbow9015KGBox = [8];
% CalibrationDataSet.trial(Elbow9015KGBox).UpperArmAngle = -90;
% CalibrationDataSet.trial(Elbow9015KGBox).ForearmAngle = 0;
% CalibrationDataSet.trial(Elbow9015KGBox).BoxMass = 15;
% 

FullFlexionTrialNumber = [9];
CalibrationDataSet.trial(FullFlexionTrialNumber).UpperArmAngle = deg2rad(-90);
CalibrationDataSet.trial(FullFlexionTrialNumber).ForearmAngle = deg2rad(-90);
CalibrationDataSet.trial(FullFlexionTrialNumber).BoxMass = 0;

%% Inverse Dynamics 
InverseDynamicsOnSiteScript

%% Passive Moment estimation
PassiveOnSiteScript

%% Regerssion Model
CalibrationDatasetForRegMod = [];
for trialnum = CalibrationTrials 
    CalibrationDatasetForRegMod = [CalibrationDatasetForRegMod ; ...
                                   CalibrationDataSet.trial(trialnum).inclination,...
                                   ...%CalibrationDataSet.trial(trialnum).AverageMyoArmband,...
                                   CalibrationDataSet.trial(trialnum).ML5S1(:,2)];
end

RMtrainedModel = fitrsvm(CalibrationDatasetForRegMod(:,1:end-1),CalibrationDatasetForRegMod(:,end),'KernelFunction','gaussian');
saveCompactModel(RMtrainedModel,"../RMtrainedModel.mat");

% [RMtrainedModel, validationRMSE] = trainRM1(CalibrationDatasetForRegMod);
% RMtrainedModel = rmfield(RMtrainedModel,'predictFcn');
% validationRMSE
% save("../src/RMtrainedModel.mat","RMtrainedModel")
% saveLearnerForCoder(RM1trainedModel.RegressionSVM,'RM1trainedModel');
% RMpredictionfunction_script

