% Create Bus for Gable DEBUG

IMUStruct = struct;
IMUStruct.Acceleration = double(zeros(3, 1));
IMUStruct.Gyroscope = double(zeros(3, 1));
IMUStruct.Quaternion = double(zeros(4, 1));

IMUIn = struct2bus(IMUStruct);
clear IMUStruct
