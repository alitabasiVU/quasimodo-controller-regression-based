% Joint_Parameters for the Quasimodo
%
% This script contains parameters for all actuators of the Quasimodo
% exoskeleton.

% Remove all variables we're going to create
clear QuasimodoJointParams sampleTime;

%% Sample Time
sampleTime = 0.001; % [seconds]

QuasimodoJointParams = struct;

%% General

% Create a struct as a basis for all joints. Then override values per joint
% to keep this list somewhat DRY.

% General comments should also only be placed here!

Joint = struct;

Joint.Motor.RatedCurrent_A = 20; % A
Joint.Motor.MaxCurrent_A = 40; % A
MotorConstant_Kv = 100; % rpm / V
Joint.Motor.MotorConstant_Nm_A = 1 / (MotorConstant_Kv * pi / 30); % Nm/A
Joint.GearboxRatio = 101;
Joint.SpringStiffness_Nm_rad = 1750; % Nm/rad

Joint.MotorEncoder.Counts_rev = 2^19; % Negative -> Negative encoder count is positive angle
Joint.MotorEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

Joint.Version = 3;

% ROMIncludesZero:  0 or 1, whether the range of motion contains encoder
%                   reading 0 and will need unwrapping.

%Joint.OutputEncoder.Counts_rev = 2^19; % counts / rev
Joint.OutputEncoder.Counts_rev = 2^14; % counts / rev update 14-3-2023

Joint.OutputEncoder.Offset = 0; % counts
Joint.OutputEncoder.ROMIncludesZero = 1;
Joint.OutputEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.003;

Joint.SpringEncoder.Counts_rev = 2^19;

Joint.SpringEncoder.Offset = 0; % counts
Joint.SpringEncoder.ROMIncludesZero = 0;
Joint.SpringEncoder.SpikeFilter.WindowGrowthRate_rad_sample = 0.01;

% Software endstop, actually the motor angle is limited (not the joint
% angle)

Joint.Limits.MotorAngleMin_rad = -1e10; % WARNING: Unlimited 
Joint.Limits.MotorAngleMax_rad = 1e10; % WARNING: Unlimited
Joint.Limits.MaxVelocity_rad_s = 100;
Joint.Limits.MaxAcceleration_rad_s2 = 20;
Joint.Limits.MaxDeceleration_rad_s2 = 200;
Joint.Limits.MaxJointTorque_Nm = 70; 
Joint.PVALimiter.DampingConstant = 200;

Joint.Guards.JointAnglePositionGuardMin_rad = Joint.Limits.MotorAngleMin_rad - 0.05;
Joint.Guards.JointAnglePositionGuardMax_rad = Joint.Limits.MotorAngleMax_rad + 0.05;
Joint.Guards.TorqueGuard_Nm = 80;
Joint.Guards.SpringDeflectionGuard_rad = (Joint.Guards.TorqueGuard_Nm + 10) / Joint.SpringStiffness_Nm_rad;

Joint.TorqueControllerType = 3; % DOB controller, do not change
Joint.DOBController.SEA_inertia = 1.9; % Identified by Cor
Joint.DOBController.SEA_damper = 15; % Identified by Cor
Joint.DOBController.System_Bandwidth_Hz = 30; % Hz
Joint.DOBController.System_Zeta = 0.7;
Joint.DOBController.DOB_FilterBandwidth_Hz = 10; % Hz
Joint.DOBController.DOB_Bandwidth_Hz = Joint.DOBController.System_Bandwidth_Hz;
Joint.DOBController.DOB_Zeta = Joint.DOBController.System_Zeta;
%Joint.DOBController.DOBGain = 0.7;
Joint.DOBController.DOBGain = 0.35;



Joint.DOBController.TorqueFFGain = 1;
Joint.DOBController.KpGainCorrection = 1;
Joint.DOBController.KdGainCorrection = 1;
Joint.DOBController.Derivative_FilterBandwidth_Hz = 160;

% As a separate parameter so that you can reduce it if this amount gives problems
Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm = abs(Joint.Motor.MaxCurrent_A * Joint.Motor.MotorConstant_Nm_A * Joint.GearboxRatio);
Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtMaxVelocity_Nm = 0.5 * Joint.AdaptiveMaxMotorTorque.MaxMotorTorqueAtZeroVelocity_Nm;
Joint.AdaptiveMaxMotorTorque.DecayVelocity_normalized = 0.5; % See comment in DynamicMaxMotorTorque block

%% Left Lumbar Flexion

LLF = Joint;

LLF.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;
LLF.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

LLF.OutputEncoder.Offset = 13885;
LLF.OutputEncoder.ROMIncludesZero = 1;
LLF.OutputEncoder.Counts_rev = Joint.OutputEncoder.Counts_rev;

LLF.OutputEncoder.Counts_rev = -2^14 % for now

LLF.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;
LLF.SpringEncoder.Offset = 272601;

LLF_range_rad=1.395; %~80 degrees

LLF.Limits.MotorAngleMin_rad = 0; 
LLF.Limits.MotorAngleMax_rad = LLF_range_rad-0.1;


%LLF.Limits.MotorAngleMin_rad = -0.35; 
%LLF.Limits.MotorAngleMax_rad = 0.57; % Mech. endstop is [0.62, -0.40]

LLF.Guards.JointAnglePositionGuardMin_rad = LLF.Limits.MotorAngleMin_rad - 0.05;
LLF.Guards.JointAnglePositionGuardMax_rad = LLF.Limits.MotorAngleMax_rad + 0.05;

QuasimodoJointParams.LLF = LLF;

%% Right Lumbar Flexion

RLF = Joint;

RLF.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;

RLF.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

RLF.OutputEncoder.Offset = 1097;

RLF.OutputEncoder.Counts_rev = -2^14 % for now

RLF.SpringEncoder.Offset = 273700;
RLF.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;

RLF_range_rad=1.376; %~79 degrees

RLF.Limits.MotorAngleMin_rad = 0; 
RLF.Limits.MotorAngleMax_rad = RLF_range_rad-0.1;

%RLF.Limits.MotorAngleMin_rad = LLF.Limits.MotorAngleMin_rad;
%RLF.Limits.MotorAngleMax_rad = LLF.Limits.MotorAngleMax_rad;

RLF.Guards.JointAnglePositionGuardMin_rad = LLF.Guards.JointAnglePositionGuardMin_rad;
RLF.Guards.JointAnglePositionGuardMax_rad = LLF.Guards.JointAnglePositionGuardMax_rad;

QuasimodoJointParams.RLF = RLF;

%% Left Hip Flexion

LHF = Joint;

LHF.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;
LHF.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;


LHF.OutputEncoder.Offset = 296100;
LHF.OutputEncoder.Counts_rev = Joint.OutputEncoder.Counts_rev;
LHF.SpringEncoder.Offset = 481680;

LHF_range_rad=2.441; %~140 degrees

LHF.Limits.MotorAngleMin_rad = 0;
LHF.Limits.MotorAngleMax_rad = LHF_range_rad-0.1; 

%LHF.Limits.MotorAngleMin_rad = -0.12;
%LHF.Limits.MotorAngleMax_rad = 1.88; % Mech. limits: [1.93, -0.17]

LHF.Guards.JointAnglePositionGuardMin_rad = LHF.Limits.MotorAngleMin_rad - 0.05;
LHF.Guards.JointAnglePositionGuardMax_rad = LHF.Limits.MotorAngleMax_rad + 0.05;

QuasimodoJointParams.LHF = LHF;

%% Right Hip Flexion

RHF = Joint;

RHF.Motor.MotorConstant_Nm_A = -Joint.Motor.MotorConstant_Nm_A;

RHF.MotorEncoder.Counts_rev = -Joint.MotorEncoder.Counts_rev;

%RHF.OutputEncoder.Offset = 289900; % encoder jump ~ 32600 counts
RHF.OutputEncoder.Offset = 12858; % Such that the endstop is -0.15 rad
RHF.OutputEncoder.Counts_rev = Joint.OutputEncoder.Counts_rev;

RHF.SpringEncoder.Offset = 88700;
RHF.SpringEncoder.Counts_rev = -Joint.SpringEncoder.Counts_rev;

RHF.Limits.MotorAngleMin_rad = LHF.Limits.MotorAngleMin_rad;
RHF.Limits.MotorAngleMax_rad = LHF.Limits.MotorAngleMax_rad;

RHF.Guards.JointAnglePositionGuardMin_rad = LHF.Guards.JointAnglePositionGuardMin_rad;
RHF.Guards.JointAnglePositionGuardMax_rad = LHF.Guards.JointAnglePositionGuardMax_rad;

QuasimodoJointParams.RHF = RHF;

%% Clean up

clear Joint LLF LHF RLF RHF
