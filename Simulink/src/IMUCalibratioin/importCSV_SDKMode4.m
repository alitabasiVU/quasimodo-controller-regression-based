function [ PacketCounter,SampleTimeFine, Quat, Acc] = importCSV_SDKMode4(FullFileName, headerLines)

sData = importdata(FullFileName,',',headerLines);
PacketCounter = [];
SampleTimeFine = single(sData.data(1:end-5,1));
Quat = single(sData.data(1:end-5,18:21));
Acc = single(sData.data(1:end-5,9:11));
Quat = single(sData.data(1:end-5,18:21));
% Gyr = single(sData.data(:,10:12));
% Mag = single(sData.data(:,13:15));

return