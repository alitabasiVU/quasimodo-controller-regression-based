function [ Qconj ] = Qconjugate(Q)

%   Determines the conjugate of a quaternion.

Qconj = [Q(:,1) -Q(:,2) -Q(:,3) -Q(:,4)];

end