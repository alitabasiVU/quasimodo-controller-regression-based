function [Qout] = Qmean(Q)

[Qout, ~] = eigs((1/size(Q,1)).*(double(Q)'*double(Q)),1);
Qout=Qout';
return