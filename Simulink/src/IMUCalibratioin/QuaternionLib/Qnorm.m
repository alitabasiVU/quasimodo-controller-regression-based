function [ Qnorm ] = Qnorm(Q)

%normalizes quaternion

Qmag = sqrt(sum(Q.^2,2));
Qnorm = Q./(Qmag .* ones(length(Qmag),4));

end