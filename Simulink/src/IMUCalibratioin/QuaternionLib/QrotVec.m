function [vectorRotated] = QrotVec(vector, Q)

vectorRotated = zeros(size(vector,1),4);
if sum(size(Q,1)) == 1
    for i=1:size(vector,1)
        vectorRotated(i,:) = Qrotate([0 vector(i,:)], Q);   
    end
    
elseif size(Q,1) == size(vector,1)
    for i=1:size(vector,1)
        vectorRotated(i,:) = Qrotate([0 vector(i,:)], Q(i,:));
    end
end

vectorRotated = vectorRotated(:,2:4);

return