function qout = Qinterp(p,q,f,method)
%  QINTERP Calculate the interpolation between two quaternions.
%   QI = QINTERP(P,Q,F,METHOD) calculates the interpolation quaternion
%   between two normalized quaternions P and Q by interval fraction F.
%
%   Inputs:
%   P,Q:        M-by-4 matrices containing M quaternions.
%   F:          M-by-1 vector. F varies between 0 and 1.
%   METHOD:     String specifying the method for the interpolation. The
%               options for the methods are: 
%               'SLERP' - (Default) Spherical linear quaternion 
%                         interpolation. 
%               'LERP'  - Linear quaternion interpolation.
%               'NLERP' - Normalized linear interpolation.
%
%   Output:
%   QI:         M-by-4 matrix of interpolated quaternions.
%
%   Examples:
%
%   Determine the quaternion located in the middle between p = [0.7071 0 0.7071 0]
%   and q = [-0.7071 0 0.7071 0] using the 'SLERP' method:
%
%       pn = quatnormalize([0.7071 0 0.7071 0])
%       qn = quatnormalize([-0.7071 0 0.7071 0])
%       qi = Qinterp(pn,qn,0.5,'slerp')
%
%   Copyright 2015 The MathWorks, Inc.

% In case that the interpolation is bigger than 90 degrees, one of the
% quaternions must be changed to its negative equivalent so the
% interpolation is symmetric.
dotpq = dot(p',q')<0;
if any(dotpq)
    q(dotpq,:) = -q(dotpq,:);
end

% Calculate quaternion interpolation depending on the selected method
qout = zeros(size(p));
switch method
    case 'slerp'
        qout = Qmultiply(p,Qpower(Qnorm(Qmultiply(Qconjugate(p),q)),f));
    case 'lerp'
        for k=1:size(p,1)
            qout(k,:) = p(k,:)*(1-f(k))+q(k,:)*f(k);
        end
    case 'nlerp'
        for k=1:size(p,1)
            qout(k,:) = p(k,:)*(1-f(k))+q(k,:)*f(k);     
        end
        qout = Qnorm(qout);
end

end

function qout = Qpower(q,pow)
    % Calculate power
    len = size(q,1);
    qout = cell2mat(arrayfun(@(k) Qexp(pow(k)*Qlog(q(k,:))),1:len,'UniformOutput',false)');
end

function qout = Qexp(q)
    % Calculate half the rotation
    len = size(q,1);
    th = arrayfun(@(k) norm(q(k,2:4)),1:len,'UniformOutput',true);
    % Calculate exponential
    qout = reshape(cell2mat(arrayfun(@(k) exp(q(k,1))*[cos(th(k)) sin(th(k))*q(k,2:4)/th(k)],1:len,'UniformOutput',false)'),len,4);
    % Replace NaN's by 0's for singularity
    if any(th==0)
        rZero = zeros(len,3);
        qout(th==0,2:4)=rZero(th==0,:);
    end
end

function qout = Qlog(q)
    % Calculate half the rotation angle
    len = size(q,1);
    normv = arrayfun(@(k) norm(q(k,2:4)),1:len,'UniformOutput',true)';
    th = atan2(normv,q(:,1));

    % Initialize outputs
    qout = zeros(size(q));

    % Calculate logarithm
    tmp = arrayfun(@(k) th(k)*q(k,2:4)/normv(k),1:len,'UniformOutput',false);
    tmp = reshape(cell2mat(tmp'),len,3);
    qout(normv~=0,2:4)=tmp(normv~=0,:);
end