function [ Q3 ] = Qmultiply(Q1, Q2)
Q3 = nan(1,4);
%
% FORMAT INPUT:
% Q1 Array: [w i j k]
% Q2 Array: [w i j k]
% 
% FORMAT OUTPUT:
% Q3 Array: [w i j k] 

% potentially normalize the input quaternions
% Q1 = Qnorm(Q1);
% Q2 = Qnorm(Q2);

%check input
if size(Q1,1) ~= size(Q2,1)
    if size(Q1,1)==1 
        Q1=repmat(Q1,size(Q2,1),1);
    elseif size(Q2,1)==1
        Q2=repmat(Q2,size(Q1,1),1);        
    else
        error('Q1 & Q2 dimensions are inconsitent');
    end
end

Q3(:,1) = -Q1(:,2) .* Q2(:,2) - Q1(:,3) .* Q2(:,3) - Q1(:,4) .* Q2(:,4) + Q1(:,1) .* Q2(:,1);
Q3(:,2) =  Q1(:,2) .* Q2(:,1) + Q1(:,3) .* Q2(:,4) - Q1(:,4) .* Q2(:,3) + Q1(:,1) .* Q2(:,2);
Q3(:,3) = -Q1(:,2) .* Q2(:,4) + Q1(:,3) .* Q2(:,1) + Q1(:,4) .* Q2(:,2) + Q1(:,1) .* Q2(:,3);
Q3(:,4) =  Q1(:,2) .* Q2(:,3) - Q1(:,3) .* Q2(:,2) + Q1(:,4) .* Q2(:,1) + Q1(:,1) .* Q2(:,4);

% potentially normalize output quaternion
% Q3 = Qnorm(Q3);

end