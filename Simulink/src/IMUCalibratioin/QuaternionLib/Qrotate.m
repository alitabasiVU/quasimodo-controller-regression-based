function [ Qrot ] = Qrotate(Q1, Q2)

% Rotates Q1 by Q2:
% Qrot = Q2*Q1*Q2^-1 where Q2^-1 is the conjugate of Q2

% potentially normalize input quaternions
% Q1 = Qnorm(Q1);
% Q2 = Qnorm(Q2);

Qrot = Qmultiply(Qmultiply(Q2, Q1),Qconjugate(Q2));

end


