function [ Qinv ] = Qinverse(Q)

% determine inverse of quaternion Q

Qmag = sqrt(Q(:,1).^2 + Q(:,2).^2 + Q(:,3).^2 + Q(:,4).^2);
Qconj = Qconjugate(Q);

Qinv = Qconj./(Qmag.^2);

end