function [SampleTimeFine, Address ,Quat, Acc] = importCSVwithSensorAddress(FullFileName, headerLines)
% This Function is compatible with xsens dot server app output data
% structure
sData = importdata(FullFileName,',',headerLines);
Address = sData.textdata(:,2);
% PacketCounter = [];%str2double(sData.textdata(:,1));
SampleTimeFine = str2double(sData.textdata(:,1));
Quat = single(sData.data(:,1:4));
Acc = single(sData.data(:,5:7));
% Gyr = single(sData.data(:,10:12));
% Mag = single(sData.data(:,13:15));

return