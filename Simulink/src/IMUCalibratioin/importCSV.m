function [PacketCounter, SampleTimeFine, Quat, Acc] = importCSV(FullFileName, headerLines)

sData = importdata(FullFileName,',',headerLines);

PacketCounter = single(sData.data(:,1));
SampleTimeFine = single(sData.data(:,2));
Quat = single(sData.data(:,3:6));
Acc = single(sData.data(:,7:9));
% Gyr = single(sData.data(:,10:12));
% Mag = single(sData.data(:,13:15));

return