function [order] = getOrder(DF)

% order to sort output of quat2angle
DF = lower(DF);

for i=1:3
    if strcmp(DF(i),'x')
        order(1,1) = i;
    elseif strcmp(DF(i),'y')
        order(1,2) = i;
    elseif strcmp(DF(i),'z')
        order(1,3) = i;
    end
end
