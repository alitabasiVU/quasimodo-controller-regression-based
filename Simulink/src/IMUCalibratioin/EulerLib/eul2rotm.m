function [RM] = eul2rotm(A, DF)

% Input: 
% A = X Y Z angles 
% DF = decomposition format (1=X; 2=Y; 3=Z);

RM=eye(3);
for i=1:3
    if DF(i) == 1
        R=RotX(A(1));
    elseif DF(i) == 2
        R=RotY(A(2));
    elseif DF(i) == 3
        R=RotZ(A(3));
    end
    RM=RM*R;
end

return