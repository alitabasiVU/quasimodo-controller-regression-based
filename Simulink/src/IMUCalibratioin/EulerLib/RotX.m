function [ Rx ] = RotX( Th1 )
%ROTX Summary of this function goes here
%   Detailed explanation goes here

Rx = [1, 0, 0
      0, cos(Th1), -sin(Th1)
      0, sin(Th1), cos(Th1)];
end

