% Script to load and process IMU data (quaternion orientation & raw IMU 
% data: linear acceleration including gravity (from accelerometer), 
% angular velocity (from gyroscope), compass/magnetic field (from magnetometer) 
% captured with two sensors at the pelvis and thorax. 
% Main outcomes: 
% (1) inclination angle over time calculated based on the acceleration data
% (2) 3D segment (ie pelvis & trunk) and joint (lumbar) angles calculated
% based on the quaternion data. The quaterion data is fused on board (on
% the sensor) and thus an output. The lumbar angles are corrected for drift
% around the vertical (global) axis (which is aligned with gravity).
% ------
% The script roughly contains the following parts:
% 0.1. add paths to folders with necessary functions 
% 0.2. add path to folder with data of subjects
% 0.3. load mat file CutData with two columns: [Nsamples cut from start,
% Nsamples cut from the end] - to correct for later start or earlier finish
% of data collection
%
% Loop over participants:
% 1. load data
% 2. sync data of the two sensors
% 3. fill gaps in quaternion data
% 4. align coordinate system of sensors with a coordinate system with its
%    vertical axis (Y) aligned with gravity and forward axis pointing 
%    forward (X; in walking direction) and lateral axis (Z) computed using  
%    the cross product of X and Y. This coordinate system is determined 
%    using the walking trial
% 5. determine acceleration-based inclination
% 6. determine quaternion-based euler angles of trunk segment, pelvis 
%    segment and lumbar joint
%
% Niels Brouwer 2021
%%%%%%%%%%%%%%%%% 
% Modificatin by Ali Tabasi
% Calibrate Sensor orientation using walking trial
% report realtime Euler Angle
% the script has been seperated into two parts: 
% 1. Calibration 
% 2. Realtime Euler angle calculation
%--------------------------------------------------------------------------

clc;
clear;
close all;

addpath('EulerLib')
addpath('QuaternionLib')
addpath('HarmonicRatioCalibration')

datapath = [cd,'/Data/']; %path to csv files
PelvisIMUAddress = 'd4:22:cd:00:43:cc';
T10IMUAddress = 'd4:22:cd:00:51:06';
Gender = 1; % male 1 , female 2
params.BodyMass = 85;
params.ThoraxLength = 0.6;

% load CutData %contains per participant the number of samples which should be cut off from start/end from recorded Trial (ie correct start/end trial)

% define struct names to store the data in (in case more files are imported
% one should add additional struct names, eg 'Measurement_2' or other names)
% see for the imported files below
% sNames = {'Static','Walk','Measurement_1'};
sNames = {'Static','Walk'};

% define which of the above structs (thus data) should be selected for
% analysis: e.g. Measurement_1 when the recorded Trial should be analysed, 
% e.g. Walk when the walking trial should be analysed
% tName = 'Bend';

% define which of the above structs (thus data) the loaded CutData applies.
% So, the length of which recorded trial can be corrected using the data in
% the CutData variable. In this case this is 'Measurement_1' since CutData
% corresponds to that measurement.
% sCutData = 'Measurement_1';
sCutData = ''; % for running the pilot data (ie not use CutData)

sub=5;

path = [datapath,'IMUCalibrationFiles','/'];
%names of files to import (might differ per participant)
% for some participants the static and/or walk were measured twice, because
% of errors during recording (not static/no straight line walking etc). The
% correct files to use were selected based on a comparison between (1) the
% flexion/extension angles calculated with the quaternion orientation and
% (2) the inclination angles calculated with the acceleration data. These
% are all calculated below.
% if sub == 3
%     files = {'Static','Walk_2','Trial'}; 
% elseif any(sub == [4 7 21])
%     files = {'Static_2','Walk_2','Trial'};
% else
files = {'Static','Walk'}; 
% %     files = {'static','walk01','FE01'}; % example for running pilot data
% end

for i=1:length(files)

%     using importCSV

    disp(['Select ' files{1,i} ' Trial for Pelvis IMU (' PelvisIMUAddress ')']);
    [CalibrationFile CalibrationFilepath]= uigetfile('*.csv',['Select ' files{1,i} ' Trial for Pelvis IMU (' PelvisIMUAddress ')'],'C:\Users\Administrator\source\IMUpython-main\Python\log');
    [~, pelv.SampleTimeFine, pelv.Quat, pelv.Acc] = importCSV_SDKMode4([CalibrationFilepath,CalibrationFile],2);
    
    disp(['Select ' files{1,i} ' Trial for Trunk IMU (' T10IMUAddress ')'])
    [CalibrationFile CalibrationFilepath]= uigetfile('*.csv',['Select ' files{1,i} ' Trial for Trunk IMU (' T10IMUAddress ')'],'C:\Users\Administrator\source\IMUpython-main\Python\log');
    [~, t10.SampleTimeFine, t10.Quat, t10.Acc] = importCSV_SDKMode4([CalibrationFilepath,CalibrationFile],2);

%     %%%%%%%%%%% To Calibrate using data captured by Xsens server App %%%%%%%%%
%     t10.SampleTimeFine = [];
%     t10.Quat = [];
%     t10.Acc = [];
%     pelv.SampleTimeFine = [];
%     pelv.Quat = [];
%     pelv.Acc = [];
%     [CalibrationFile CalibrationFilepath]= uigetfile('*.csv',['Select ' files{1,i} ' Trial']);
%     [CalibrationData.SampleTimeFine,CalibrationData.Address,CalibrationData.Quat,CalibrationData.Acc]  =  importCSVwithSensorAddress([CalibrationFilepath,CalibrationFile],8);
%     for tsample = 8: length(CalibrationData.SampleTimeFine)-8
%        if  strcmp(CalibrationData.Address{tsample},T10IMUAddress) 
%            t10.SampleTimeFine = [t10.SampleTimeFine;CalibrationData.SampleTimeFine(tsample)];
%            t10.Quat = [t10.Quat;CalibrationData.Quat(tsample,:)];  
%            t10.Acc = [t10.Acc;CalibrationData.Acc(tsample,:)];
%        elseif  strcmp(CalibrationData.Address{tsample},PelvisIMUAddress)
%            pelv.SampleTimeFine = [pelv.SampleTimeFine;CalibrationData.SampleTimeFine(tsample)];
%            pelv.Quat = [pelv.Quat;CalibrationData.Quat(tsample,:)];  
%            pelv.Acc = [pelv.Acc;CalibrationData.Acc(tsample,:)];
%        else
%            errordlg('Wrong Sensor used');
%            break
%        end
%     end
%     %%%%%%%%%%%
%     %%%%%%%%%%% To Calibrate using data captured by Xsens phone App %%%%%%%%%

%     % using importCSV
%     [~, pelv.SampleTimeFine, pelv.Quat, pelv.Acc] = importCSV([path,'Pelvis_',files{1,i},'.csv'],8);
%     [~, t10.SampleTimeFine, t10.Quat, t10.Acc] = importCSV([path,'T10_',files{1,i},'.csv'],8);
    %%%%%%%%%%%%
    % define start of data capture (past the nans and zeros)
    % sometimes the first (few) rows of STF are NaNs or the first (few)
    % rows of the acceleration data are zeros
    iStartPe = find( logical( ~isnan(pelv.SampleTimeFine(:,1)) .* (pelv.Acc(:,1)~=0) ),1);
    iStartT10 = find( logical( ~isnan(t10.SampleTimeFine(:,1)) .* (t10.Acc(:,1)~=0) ),1);

    % create time series (internal clock of IMUs is reset every about 1.2
    % hours, hence the correction for clock reset)
    % This is the exact time of each sample. If your carefully have a look
    % this does not necessary exactly match the sample frequency
    t10.SampleTimeFine = SFTcorrection(t10.SampleTimeFine);
    pelv.SampleTimeFine = SFTcorrection(pelv.SampleTimeFine);

    % construct time series
    tPe = single((pelv.SampleTimeFine(:,1)-pelv.SampleTimeFine(iStartPe,1))./10.^4);
    tT10 = single((t10.SampleTimeFine(:,1)-t10.SampleTimeFine(iStartT10,1))./10.^4);
    % calculate average sample frequency
    fs = round(1./ nanmean(diff(pelv.SampleTimeFine(:,1))).*10.^4);   

    % syncing the sensors. Only data from the time window where both sensors measures is kept 
    iStart = min([iStartPe,iStartT10]);
    minlength  = min([length(tPe(iStart:end)),length(tT10(iStart:end))]);
    time = [tPe(iStart:minlength),tT10(iStart:minlength)];
    paramnames = fieldnames(pelv);
    for p=2:length(paramnames)
        data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.(char(paramnames(p,1))) = pelv.(char(paramnames(p,1)))(iStart:minlength,:);
        data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.(char(paramnames(p,1))) = t10.(char(paramnames(p,1)))(iStart:minlength,:);
    end

    [~,last]  = max(time(1,:)); % determine sensor that started measuring last
    [~,first] = min(time(1,:)); % determine sensor that started measuring first
    [~,index] = min(abs(time(:,first)-time(1,last)));
    iStart=iStart-1;
    % the following part cuts (according to CutData) the designated file,
    % so start or end of measurement is possibly corrected.
    if strcmp(sNames{1,i},sCutData)
        TrialStart = CutData(sub,1)*fs;
        if CutData(sub,2)~=0; TrialEnd = CutData(sub,2)*fs;
        else; TrialEnd = size(time(:,1),1);
        end
    else 
        TrialStart = 1; TrialEnd = size(time(:,1),1);
    end
    if first == 1
        PelvWin = (TrialStart+index-1):TrialEnd;
        T10Win = TrialStart:(TrialEnd-index+1);
    else
        PelvWin = TrialStart:(TrialEnd-index-1);
        T10Win = (TrialStart+index+1):TrialEnd;
    end
    data.(['Sub',num2str(sub)]).(sNames{1,i}).time = time(PelvWin,1)-time(PelvWin(1),1);
    data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat =  data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat(PelvWin,:);
    data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat =  data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat(T10Win,:);
    data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Acc =  data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Acc(PelvWin,:);
    data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Acc =  data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Acc(T10Win,:);

    
    data.(['Sub',num2str(sub)]).(sNames{1,i}).fs = round(1/diff(data.(['Sub',num2str(sub)]).(sNames{1,i}).time(1:2,1)));
    
    %----------- end of sync code ----
    
    % fix quaternion NaN and numbers larger than 1 or -1; normalize to get
    % unit quaternions. In short: gaps or not valid quaternion data is
    % corrected using quaternion interpolation (quite complex)
    iPe = find(sum(isnan(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat)+(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat>1)+(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat<-1),2));
    it10 = find(sum(isnan(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat)+(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat>1)+(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat<-1),2));
    if ~isempty(iPe);  for j=1:length(iPe); data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat(iPe(j),:) = Qinterp(Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat(iPe(j)-1,:)),Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat(iPe(j)+1,:)),0.5,'slerp'); end; end
    if ~isempty(it10);  for j=1:length(it10); data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat(it10(j),:) = Qinterp(Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat(it10(j)-1,:)),Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat(it10(j)+1,:)),0.5,'slerp'); end; end
    data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat = Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).Pelvis.Quat);
    data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat = Qnorm(data.(['Sub',num2str(sub)]).(sNames{1,i}).T10.Quat);

end

%% Align sensors using acc data from walking trial

% Calibration based on frequencies/harmonics along axes during walking. A
% rotation matrix is calculated to align the coordinate systems of the IMUs
% with the segment coordinate systems.
[~,RotationMatrixTpe,~] = RealignSensorSignalHRAmp(double(data.(['Sub',num2str(sub)]).Walk.Pelvis.Acc), data.(['Sub',num2str(sub)]).Walk.fs, 0);
[~,RotationMatrixTt10,~] = RealignSensorSignalHRAmp(double(data.(['Sub',num2str(sub)]).Walk.T10.Acc), data.(['Sub',num2str(sub)]).Walk.fs, 0);

% The output rotation matrix does not match the axis orientation in the 
% current script. Therefore, correction to get: X forward; Y vertical; Z lateral right
RotationMatrixTpe = RotationMatrixTpe*RotZ(pi/2)*RotY(pi/2);
RotationMatrixTt10 = RotationMatrixTt10*RotZ(pi/2)*RotY(pi/2);
% convert rotation matrix to quaternion
Qpe = rotm2quat(RotationMatrixTpe);
Qt10 = rotm2quat(RotationMatrixTt10);

%store data in struct
% data.(['Sub',num2str(sub)]).(tName).Pelvis.RotMatT = RotationMatrixTpe;
% data.(['Sub',num2str(sub)]).(tName).T10.RotMatT = RotationMatrixTt10;
% data.(['Sub',num2str(sub)]).(tName).Pelvis.Qinv = Qpe;
% data.(['Sub',num2str(sub)]).(tName).T10.Qinv = Qt10;


%% IMU orientation - Quaternion calculation

% this part is threefold:
% (1) calculate average static 3D orientation of static (anatomical 
%     neutral) posture and rotate this average 3D orientation expressed in 
%     the global earth coordinate system to the corresponding segment
%     coordinate system (with the quaternion rotation calculated based on 
%     the walking trial with the alignment procedure)
% (2) rotate the time series of the 3D orientation of the trial which is 
%     being analysed (eg a surgical procedure) from the global earth 
%     coordinate system to the corresponding segment coordinate system
%     (again: with the quaternion rotation calculated based on the walking  
%     trial with the alignment procedure)
% (3) calculate the 3D orientation relative to the static anatomical 
%     neutral posture over time of the trial which is being analysed. 
%     So, (2) relative to (1).
% Outcome of (3): 3D orientation of thorax and pelvis (seperately) relative 
% to static posture expressed in corresponding segment coordinate system

% 1
data.(['Sub',num2str(sub)]).Static.Pelvis.QuatRot = Qmultiply(Qmean(data.(['Sub',num2str(sub)]).Static.Pelvis.Quat),Qpe);
data.(['Sub',num2str(sub)]).Static.T10.QuatRot = Qmultiply(Qmean(data.(['Sub',num2str(sub)]).Static.T10.Quat),Qt10);
CalibVars.Static.Pelvis.QuatRot = data.(['Sub',num2str(sub)]).Static.Pelvis.QuatRot;
CalibVars.Static.T10.QuatRot = data.(['Sub',num2str(sub)]).Static.T10.QuatRot;
CalibVars.Walk.Pelvis.Qpe = Qpe;
CalibVars.Walk.T10.Qt10 = Qt10;


%%% 
save(['../IMUCalibrationResults'],'CalibVars')
% csvwrite('CalibVars.txt',[CalibVars.Static.Pelvis.QuatRot CalibVars.Static.T10.QuatRot CalibVars.Walk.Pelvis.Qpe CalibVars.Walk.T10.Qt10 ]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%  END of CALIBRATION %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%  Passive curve calibration %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear thorax pelvis lumbar
clear AlignedOffsetLumbarQuat AlignedOffsetPelvisQuat AlignedOffsetT10Quat

disp(['Select Full Flexion Trial for Trunk IMU (' T10IMUAddress ')'])
[CalibrationFile CalibrationFilepath]= uigetfile('*.csv',['Select Full Flexion Trial for Trunk IMU (' T10IMUAddress ')'],'C:\Users\Administrator\source\IMUpython-main\Python\log');
[~, FFtrial.t10.SampleTimeFine, FFtrial.t10.Quat, FFtrial.t10.Acc] = importCSV_SDKMode4([CalibrationFilepath,CalibrationFile],2);


disp(['Select Full Flexion Trial for Pelvis IMU (' PelvisIMUAddress ')'])
[CalibrationFile CalibrationFilepath]= uigetfile('*.csv',['Select Full Flexion Trial for Pelvis IMU (' PelvisIMUAddress ')'],'C:\Users\Administrator\source\IMUpython-main\Python\log');
[~, FFtrial.pelv.SampleTimeFine, FFtrial.pelv.Quat, FFtrial.pelv.Acc] = importCSV_SDKMode4([CalibrationFilepath,CalibrationFile],2);
for timestep = 1: min(length(FFtrial.t10.Quat),length(FFtrial.pelv.Quat))
    AlignedOffsetT10Quat(timestep,:) =      Qmultiply(Qinverse(CalibVars.Static.T10.QuatRot),   Qmultiply(FFtrial.t10.Quat(timestep,:), CalibVars.Walk.T10.Qt10));
    AlignedOffsetPelvisQuat(timestep,:) =   Qmultiply(Qinverse(CalibVars.Static.Pelvis.QuatRot),Qmultiply(FFtrial.pelv.Quat(timestep,:),CalibVars.Walk.Pelvis.Qpe));
    AlignedOffsetLumbarQuat(timestep,:) =   Qmultiply(Qinverse(AlignedOffsetPelvisQuat(timestep,:)),AlignedOffsetT10Quat(timestep,:));
end

DecompFormat = 'yxz';
[thorax(:,1),thorax(:,2),thorax(:,3)] = quat2angle(AlignedOffsetT10Quat,DecompFormat);
[pelvis(:,1),pelvis(:,2),pelvis(:,3)] = quat2angle(AlignedOffsetPelvisQuat,DecompFormat);
[lumbar(:,1),lumbar(:,2),lumbar(:,3)] = quat2angle(AlignedOffsetLumbarQuat,DecompFormat);


TrunkInclination = thorax(:,3);
LumbarAngle = lumbar(:,3);
PelvisAngle = pelvis(:,3);

% Calculate Lumbar angle based on relative orientation of trunk and pelvis in flexion/extension euler angles
LumbarAngle = TrunkInclination - PelvisAngle;


[FlexionRelaxationAngle, i_maxLumbarAngle] = max(LumbarAngle) ;
TrunkInclination_i_maxLumbarAngle = TrunkInclination(i_maxLumbarAngle);

FlexionRelaxationAngle = rad2deg(FlexionRelaxationAngle);
TrunkInclination_i_maxLumbarAngle = rad2deg(TrunkInclination_i_maxLumbarAngle);
figure, plot (LumbarAngle*180/pi)
ylabel('Lumbar Angle (degrees)')
%%%%%%%%%%%%%%% Constant values based on  Leva 1996 %%%%%%%%%%%%%%%%%%%
SegmentMass = ...        % % of body mass, From de Leva 1996 adjustments to Zatsiorsky J. Biomechanics 29 
    [  6.68 6.94         % Head
       42.57 43.46       % Trunk
       2.55 2.71         % Upper Arm
       1.38 1.62         % Forearm
       0.56 0.61         % Hand
    ];
SegmentMass = SegmentMass/100;

SegmentCMPosition =  ...    % % of segment length, From de Leva 1996 adjustments to Zatsiorsky J. Biomechanics 29 
    [ 1-.4841 1-.5002       % Head , ratios are from Vert to Cerv, 1- is to calculate CM position from Cerv to Vert
     .46 .42                % Trunk, ratio is the distance from Iliospinale (~L5S1 Level) to Cervical
    ];
g = [0 0 -9.81] ;

d_L5S1_TrunkCOM = [ SegmentCMPosition(2,Gender)*params.ThoraxLength*sind(TrunkInclination_i_maxLumbarAngle),...    % distance in x direction
    0,...                                                                                        % distance in y direction
    SegmentCMPosition(2,Gender)*params.ThoraxLength*cosd(TrunkInclination_i_maxLumbarAngle),...                    % distance in z direction
    ];

d_L5S1_Shoulder = [ params.ThoraxLength*sind(TrunkInclination_i_maxLumbarAngle),...     % distance in x direction
    0,...                                                             % distance in y direction
    params.ThoraxLength*cosd(TrunkInclination_i_maxLumbarAngle),...                     % distance in z direction
    ];

d_L5S1_HeadCOM = [ (SegmentCMPosition(1,Gender)*0.4*params.ThoraxLength+params.ThoraxLength)*sind(TrunkInclination_i_maxLumbarAngle),...    % distance in x direction
    0,...                                                                                                                 % distance in y direction
    (SegmentCMPosition(1,Gender)*0.4*params.ThoraxLength+params.ThoraxLength)*cosd(TrunkInclination_i_maxLumbarAngle),...                   % distance in z direction
    ];

ML5S1 = [];
ML5S1 = cross(d_L5S1_HeadCOM,params.BodyMass*SegmentMass(1,Gender)*g) +...
        cross(d_L5S1_TrunkCOM,params.BodyMass*SegmentMass(2,Gender)*g) +...
        2*cross(d_L5S1_Shoulder,params.BodyMass*(SegmentMass(3,Gender)+SegmentMass(4,Gender)+SegmentMass(5,Gender))*g);
NetMomentPrediction = ML5S1(2);
FlexionRelaxationMoment = NetMomentPrediction;
angle = [deg2rad([0 10 15 20  FlexionRelaxationAngle])];
PassiveMomentParams = polyfit(angle, [0 0 0 0  FlexionRelaxationMoment],4);
save('../../src/PassiveParams.mat','PassiveMomentParams');

figure, plot([0:FlexionRelaxationAngle],polyval(  PassiveMomentParams, deg2rad([0:FlexionRelaxationAngle])))
xlabel('Lumbar Angle (degrees)')
ylabel('Passive Moment')
