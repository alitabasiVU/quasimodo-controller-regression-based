function [SFT] = SFTcorrection(SFT)

% internal clock of IMUs is reset every about 1.2 hours, hence the 
% correction for the sample fine time array

iClockReset = find(diff(SFT)<0);
if ~isempty(iClockReset) % in case of clock reset
    ClockReset = cumsum(SFT(iClockReset));
    iClockReset = [iClockReset;length(SFT)];
    for i=1:length(ClockReset)
        SFT(iClockReset(i)+1:iClockReset(i+1),:) = SFT(iClockReset(i)+1:iClockReset(i+1),:)+ClockReset(i);
    end
end

end