/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * readfile_mexutil.c
 *
 * Code generation for function 'readfile_mexutil'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "readfile.h"
#include "readfile_mexutil.h"

/* Function Definitions */
real_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *f_feval, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(f_feval), &thisId);
  emlrtDestroyArray(&f_feval);
  return y;
}

real_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = j_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

const mxArray *feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                     emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  const mxArray *m31;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(sp, 1, &m31, 2, pArrays, "feval", true, location);
}

real_T j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

/* End of code generation (readfile_mexutil.c) */
