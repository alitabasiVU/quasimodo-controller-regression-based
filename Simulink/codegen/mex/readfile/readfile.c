/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * readfile.c
 *
 * Code generation for function 'readfile'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "readfile.h"
#include "readfile_emxutil.h"
#include "fclose.h"
#include "fread.h"
#include "readfile_mexutil.h"
#include "readfile_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 64,    /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 55,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 46,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 41,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 40,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 25,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 22,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 18,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo i_emlrtRSI = { 14,  /* lineNo */
  "readfile",                          /* fcnName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 51,  /* lineNo */
  "fopen",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fopen.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 35,  /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 35,  /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 38,  /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 99,    /* lineNo */
  17,                                  /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static emlrtMCInfo b_emlrtMCI = { 10,  /* lineNo */
  14,                                  /* colNo */
  "fseek",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fseek.m"/* pName */
};

static emlrtMCInfo c_emlrtMCI = { 10,  /* lineNo */
  16,                                  /* colNo */
  "ftell",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\ftell.m"/* pName */
};

static emlrtMCInfo d_emlrtMCI = { 60,  /* lineNo */
  18,                                  /* colNo */
  "fprintf",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pName */
};

static emlrtRTEInfo emlrtRTEI = { 66,  /* lineNo */
  1,                                   /* colNo */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 47,/* lineNo */
  12,                                  /* colNo */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 4, /* lineNo */
  14,                                  /* colNo */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pName */
};

static emlrtBCInfo emlrtBCI = { 1,     /* iFirst */
  65536,                               /* iLast */
  47,                                  /* lineNo */
  12,                                  /* colNo */
  "buffer",                            /* aName */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { 1,   /* iFirst */
  65536,                               /* iLast */
  47,                                  /* lineNo */
  18,                                  /* colNo */
  "buffer",                            /* aName */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m",/* pName */
  0                                    /* checkKind */
};

static emlrtECInfo emlrtECI = { -1,    /* nDims */
  47,                                  /* lineNo */
  5,                                   /* colNo */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m"/* pName */
};

static emlrtBCInfo c_emlrtBCI = { 1,   /* iFirst */
  65536,                               /* iLast */
  66,                                  /* lineNo */
  19,                                  /* colNo */
  "buffer",                            /* aName */
  "readfile",                          /* fName */
  "C:\\Users\\Administrator\\source\\quasimodo-controller-regression-based20092022_testedXsensBased\\Simulink\\readfile.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRSInfo w_emlrtRSI = { 10,  /* lineNo */
  "ftell",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\ftell.m"/* pathName */
};

static emlrtRSInfo y_emlrtRSI = { 60,  /* lineNo */
  "fprintf",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fprintf.m"/* pathName */
};

static emlrtRSInfo ab_emlrtRSI = { 99, /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtRSInfo bb_emlrtRSI = { 10, /* lineNo */
  "fseek",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fseek.m"/* pathName */
};

/* Function Declarations */
static const mxArray *b_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, emlrtMCInfo *location);
static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location);
static const mxArray *d_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, const mxArray *f, emlrtMCInfo *
  location);

/* Function Definitions */
static const mxArray *b_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, emlrtMCInfo *location)
{
  const mxArray *pArrays[3];
  const mxArray *m34;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  return emlrtCallMATLABR2012b(sp, 1, &m34, 3, pArrays, "feval", true, location);
}

static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location)
{
  const mxArray *pArrays[4];
  const mxArray *m35;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  return emlrtCallMATLABR2012b(sp, 1, &m35, 4, pArrays, "feval", true, location);
}

static const mxArray *d_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, const mxArray *f, emlrtMCInfo *
  location)
{
  const mxArray *pArrays[5];
  const mxArray *m36;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  pArrays[4] = f;
  return emlrtCallMATLABR2012b(sp, 1, &m36, 5, pArrays, "feval", true, location);
}

void readfile(const emlrtStack *sp, const char_T filename_data[], const int32_T
              filename_size[2], emxArray_char_T *y)
{
  const mxArray *b_y;
  const mxArray *m0;
  static const int32_T iv0[2] = { 1, 5 };

  static const char_T u[5] = { 'f', 'o', 'p', 'e', 'n' };

  const mxArray *c_y;
  const mxArray *m1;
  const mxArray *d_y;
  const mxArray *m2;
  real_T f;
  const mxArray *m3;
  static const int32_T iv1[2] = { 1, 5 };

  static const char_T b_u[5] = { 'f', 's', 'e', 'e', 'k' };

  const mxArray *m4;
  const mxArray *m5;
  const mxArray *e_y;
  const mxArray *m6;
  static const int32_T iv2[2] = { 1, 3 };

  static const char_T origin[3] = { 'e', 'o', 'f' };

  const mxArray *m7;
  static const int32_T iv3[2] = { 1, 5 };

  static const char_T c_u[5] = { 'f', 't', 'e', 'l', 'l' };

  const mxArray *m8;
  real_T position;
  int32_T i0;
  const mxArray *m9;
  static const int32_T iv4[2] = { 1, 5 };

  const mxArray *m10;
  const mxArray *m11;
  const mxArray *m12;
  static const int32_T iv5[2] = { 1, 3 };

  static const char_T b_origin[3] = { 'b', 'o', 'f' };

  uint8_T buffer[65536];
  int32_T remaining;
  int32_T b_index;
  emxArray_real_T *dataRead;
  emxArray_uint16_T *r0;
  boolean_T exitg1;
  int32_T qY;
  real_T nread;
  const mxArray *m13;
  static const int32_T iv6[2] = { 1, 7 };

  static const char_T d_u[7] = { 'f', 'p', 'r', 'i', 'n', 't', 'f' };

  const mxArray *m14;
  const mxArray *m15;
  static const int32_T iv7[2] = { 1, 60 };

  static const char_T e_u[60] = { 'A', 't', 't', 'e', 'm', 'p', 't', ' ', 't',
    'o', ' ', 'r', 'e', 'a', 'd', ' ', 'f', 'i', 'l', 'e', ' ', 'w', 'h', 'i',
    'c', 'h', ' ', 'i', 's', ' ', 'b', 'i', 'g', 'g', 'e', 'r', ' ', 't', 'h',
    'a', 'n', ' ', 'i', 'n', 't', 'e', 'r', 'n', 'a', 'l', ' ', 'b', 'u', 'f',
    'f', 'e', 'r', '.', '\\', 'n' };

  int32_T i1;
  int32_T loop_ub;
  const mxArray *m16;
  static const int32_T iv8[2] = { 1, 7 };

  const mxArray *m17;
  const mxArray *m18;
  static const int32_T iv9[2] = { 1, 60 };

  static const char_T f_u[60] = { 'C', 'u', 'r', 'r', 'e', 'n', 't', ' ', 'b',
    'u', 'f', 'f', 'e', 'r', ' ', 's', 'i', 'z', 'e', ' ', 'i', 's', ' ', '%',
    'd', ' ', 'b', 'y', 't', 'e', 's', ' ', 'a', 'n', 'd', ' ', 'f', 'i', 'l',
    'e', ' ', 's', 'i', 'z', 'e', ' ', 'i', 's', ' ', '%', 'd', ' ', 'b', 'y',
    't', 'e', 's', '.', '\\', 'n' };

  const mxArray *m19;
  uint8_T u0;
  const mxArray *f_y;
  const mxArray *m20;
  static const int32_T iv10[2] = { 1, 7 };

  const mxArray *m21;
  const mxArray *m22;
  static const int32_T iv11[2] = { 1, 31 };

  static const char_T g_u[31] = { 'C', 'o', 'u', 'l', 'd', ' ', 'n', 'o', 't',
    ' ', 'r', 'e', 'a', 'd', ' ', 'f', 'r', 'o', 'm', ' ', 'f', 'i', 'l', 'e',
    ':', ' ', '%', 'd', '.', '\\', 'n' };

  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);

  /*  y = readfile(filename) */
  /*  Read file 'filename' and return a MATLAB string with the contents */
  /*  of the file. */
  /*  Put class and size constraints on function input. */
  /*  Call fopen(filename 'r'), but we need to convert the MATLAB */
  /*  string into a C type string (which is the same string with the */
  /*  NUL (\0) string terminator). */
  st.site = &i_emlrtRSI;
  b_st.site = &j_emlrtRSI;
  c_st.site = &k_emlrtRSI;
  b_y = NULL;
  m0 = emlrtCreateCharArray(2, iv0);
  emlrtInitCharArrayR2013a(&c_st, 5, m0, &u[0]);
  emlrtAssign(&b_y, m0);
  c_y = NULL;
  m1 = emlrtCreateCharArray(2, filename_size);
  emlrtInitCharArrayR2013a(&c_st, filename_size[1], m1, &filename_data[0]);
  emlrtAssign(&c_y, m1);
  d_y = NULL;
  m2 = emlrtCreateString1('r');
  emlrtAssign(&d_y, m2);
  d_st.site = &ab_emlrtRSI;
  f = c_emlrt_marshallIn(&d_st, b_feval(&d_st, b_y, c_y, d_y, &emlrtMCI),
    "feval");

  /*  Call fseek(f, 0, SEEK_END) to set file position to the end of */
  /*  the file. */
  st.site = &h_emlrtRSI;
  b_y = NULL;
  m3 = emlrtCreateCharArray(2, iv1);
  emlrtInitCharArrayR2013a(&st, 5, m3, &b_u[0]);
  emlrtAssign(&b_y, m3);
  c_y = NULL;
  m4 = emlrtCreateDoubleScalar(f);
  emlrtAssign(&c_y, m4);
  d_y = NULL;
  m5 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&d_y, m5);
  e_y = NULL;
  m6 = emlrtCreateCharArray(2, iv2);
  emlrtInitCharArrayR2013a(&st, 3, m6, &origin[0]);
  emlrtAssign(&e_y, m6);
  b_st.site = &bb_emlrtRSI;
  c_emlrt_marshallIn(&b_st, c_feval(&b_st, b_y, c_y, d_y, e_y, &b_emlrtMCI),
                     "feval");

  /*  Call ftell(f) which will return the length of the file in bytes */
  /*  (as current file position is at the end of the file). */
  st.site = &g_emlrtRSI;
  b_y = NULL;
  m7 = emlrtCreateCharArray(2, iv3);
  emlrtInitCharArrayR2013a(&st, 5, m7, &c_u[0]);
  emlrtAssign(&b_y, m7);
  c_y = NULL;
  m8 = emlrtCreateDoubleScalar(f);
  emlrtAssign(&c_y, m8);
  b_st.site = &w_emlrtRSI;
  position = c_emlrt_marshallIn(&b_st, feval(&b_st, b_y, c_y, &c_emlrtMCI),
    "feval");
  position = muDoubleScalarRound(position);
  if (position < 2.147483648E+9) {
    if (position >= -2.147483648E+9) {
      i0 = (int32_T)position;
    } else {
      i0 = MIN_int32_T;
    }
  } else if (position >= 2.147483648E+9) {
    i0 = MAX_int32_T;
  } else {
    i0 = 0;
  }

  /*  Reset current file position */
  st.site = &f_emlrtRSI;
  b_y = NULL;
  m9 = emlrtCreateCharArray(2, iv4);
  emlrtInitCharArrayR2013a(&st, 5, m9, &b_u[0]);
  emlrtAssign(&b_y, m9);
  c_y = NULL;
  m10 = emlrtCreateDoubleScalar(f);
  emlrtAssign(&c_y, m10);
  d_y = NULL;
  m11 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&d_y, m11);
  e_y = NULL;
  m12 = emlrtCreateCharArray(2, iv5);
  emlrtInitCharArrayR2013a(&st, 3, m12, &b_origin[0]);
  emlrtAssign(&e_y, m12);
  b_st.site = &bb_emlrtRSI;
  c_emlrt_marshallIn(&b_st, c_feval(&b_st, b_y, c_y, d_y, e_y, &b_emlrtMCI),
                     "feval");

  /*  Initialize a buffer */
  memset(&buffer[0], 0, sizeof(uint8_T) << 16);

  /*  Remaining is the number of bytes to read (from the file) */
  remaining = i0;

  /*  Index is the current position to read into the buffer */
  b_index = 1;
  emxInit_real_T(sp, &dataRead, 1, &c_emlrtRTEI, true);
  emxInit_uint16_T(sp, &r0, 2, &c_emlrtRTEI, true);
  exitg1 = false;
  while ((!exitg1) && (remaining > 0)) {
    /*  Buffer overflow? */
    if (b_index > MAX_int32_T - remaining) {
      qY = MAX_int32_T;
    } else {
      qY = remaining + b_index;
    }

    if (qY > 65536) {
      st.site = &e_emlrtRSI;
      b_st.site = &l_emlrtRSI;
      b_y = NULL;
      m13 = emlrtCreateCharArray(2, iv6);
      emlrtInitCharArrayR2013a(&b_st, 7, m13, &d_u[0]);
      emlrtAssign(&b_y, m13);
      c_y = NULL;
      m14 = emlrtCreateDoubleScalar(1.0);
      emlrtAssign(&c_y, m14);
      d_y = NULL;
      m15 = emlrtCreateCharArray(2, iv7);
      emlrtInitCharArrayR2013a(&b_st, 60, m15, &e_u[0]);
      emlrtAssign(&d_y, m15);
      c_st.site = &y_emlrtRSI;
      c_emlrt_marshallIn(&c_st, b_feval(&c_st, b_y, c_y, d_y, &d_emlrtMCI),
                         "feval");
      st.site = &d_emlrtRSI;
      b_st.site = &m_emlrtRSI;
      b_y = NULL;
      m16 = emlrtCreateCharArray(2, iv8);
      emlrtInitCharArrayR2013a(&b_st, 7, m16, &d_u[0]);
      emlrtAssign(&b_y, m16);
      c_y = NULL;
      m17 = emlrtCreateDoubleScalar(1.0);
      emlrtAssign(&c_y, m17);
      d_y = NULL;
      m18 = emlrtCreateCharArray(2, iv9);
      emlrtInitCharArrayR2013a(&b_st, 60, m18, &f_u[0]);
      emlrtAssign(&d_y, m18);
      e_y = NULL;
      m19 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
      *(int32_T *)emlrtMxGetData(m19) = 65536;
      emlrtAssign(&e_y, m19);
      f_y = NULL;
      m19 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
      *(int32_T *)emlrtMxGetData(m19) = i0;
      emlrtAssign(&f_y, m19);
      c_st.site = &y_emlrtRSI;
      c_emlrt_marshallIn(&c_st, d_feval(&c_st, b_y, c_y, d_y, e_y, f_y,
        &d_emlrtMCI), "feval");
      exitg1 = true;
    } else {
      /*  Read as much as possible from the file into internal buffer */
      st.site = &c_emlrtRSI;
      b_fread(&st, f, remaining, dataRead, &nread);
      position = muDoubleScalarRound((real_T)b_index + nread);
      if (position < 2.147483648E+9) {
        if (position >= -2.147483648E+9) {
          qY = (int32_T)position;
        } else {
          qY = MIN_int32_T;
        }
      } else if (position >= 2.147483648E+9) {
        qY = MAX_int32_T;
      } else {
        qY = 0;
      }

      if (qY < -2147483647) {
        qY = MIN_int32_T;
      } else {
        qY--;
      }

      if (b_index > qY) {
        i1 = 1;
        qY = 1;
      } else {
        if ((b_index < 1) || (b_index > 65536)) {
          emlrtDynamicBoundsCheckR2012b(b_index, 1, 65536, &emlrtBCI, sp);
        }

        i1 = b_index;
        if ((qY < 1) || (qY > 65536)) {
          emlrtDynamicBoundsCheckR2012b(qY, 1, 65536, &b_emlrtBCI, sp);
        }

        qY++;
      }

      loop_ub = qY - i1;
      qY = dataRead->size[0];
      if (loop_ub != qY) {
        emlrtSubAssignSizeCheck1dR2017a(loop_ub, qY, &emlrtECI, sp);
      }

      qY = r0->size[0] * r0->size[1];
      r0->size[0] = 1;
      r0->size[1] = loop_ub;
      emxEnsureCapacity_uint16_T(sp, r0, qY, &b_emlrtRTEI);
      for (qY = 0; qY < loop_ub; qY++) {
        r0->data[qY] = (uint16_T)((i1 + qY) - 1);
      }

      loop_ub = r0->size[0] * r0->size[1];
      for (i1 = 0; i1 < loop_ub; i1++) {
        position = muDoubleScalarRound(dataRead->data[i1]);
        if (position < 256.0) {
          if (position >= 0.0) {
            u0 = (uint8_T)position;
          } else {
            u0 = 0U;
          }
        } else if (position >= 256.0) {
          u0 = MAX_uint8_T;
        } else {
          u0 = 0U;
        }

        buffer[r0->data[i1]] = u0;
      }

      position = muDoubleScalarRound(nread);
      if (position < 2.147483648E+9) {
        if (position >= -2.147483648E+9) {
          qY = (int32_T)position;
        } else {
          qY = MIN_int32_T;
        }
      } else if (position >= 2.147483648E+9) {
        qY = MAX_int32_T;
      } else {
        qY = 0;
      }

      if (qY == 0) {
        /*  Nothing more to read */
        exitg1 = true;
      } else {
        /*  Did something went wrong when reading? */
        if (qY < 0) {
          st.site = &b_emlrtRSI;
          b_st.site = &m_emlrtRSI;
          b_y = NULL;
          m20 = emlrtCreateCharArray(2, iv10);
          emlrtInitCharArrayR2013a(&b_st, 7, m20, &d_u[0]);
          emlrtAssign(&b_y, m20);
          c_y = NULL;
          m21 = emlrtCreateDoubleScalar(1.0);
          emlrtAssign(&c_y, m21);
          d_y = NULL;
          m22 = emlrtCreateCharArray(2, iv11);
          emlrtInitCharArrayR2013a(&b_st, 31, m22, &g_u[0]);
          emlrtAssign(&d_y, m22);
          e_y = NULL;
          m19 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
          *(int32_T *)emlrtMxGetData(m19) = qY;
          emlrtAssign(&e_y, m19);
          c_st.site = &y_emlrtRSI;
          c_emlrt_marshallIn(&c_st, c_feval(&c_st, b_y, c_y, d_y, e_y,
            &d_emlrtMCI), "feval");
          exitg1 = true;
        } else {
          /*  Update state variables */
          remaining -= qY;
          if ((b_index < 0) && (qY < MIN_int32_T - b_index)) {
            b_index = MIN_int32_T;
          } else if ((b_index > 0) && (qY > MAX_int32_T - b_index)) {
            b_index = MAX_int32_T;
          } else {
            b_index += qY;
          }

          if (*emlrtBreakCheckR2012bFlagVar != 0) {
            emlrtBreakCheckR2012b(sp);
          }
        }
      }
    }
  }

  emxFree_uint16_T(&r0);
  emxFree_real_T(&dataRead);

  /*  Close file */
  st.site = &emlrtRSI;
  b_fclose(&st, f);
  if ((b_index < 1) || (b_index > 65536)) {
    emlrtDynamicBoundsCheckR2012b(b_index, 1, 65536, &c_emlrtBCI, sp);
  }

  i0 = y->size[0] * y->size[1];
  y->size[0] = 1;
  y->size[1] = b_index;
  emxEnsureCapacity_char_T(sp, y, i0, &emlrtRTEI);
  for (i0 = 0; i0 < b_index; i0++) {
    y->data[i0] = (int8_T)buffer[i0];
  }

  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (readfile.c) */
