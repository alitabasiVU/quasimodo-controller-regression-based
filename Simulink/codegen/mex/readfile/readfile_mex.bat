@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2018b
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2018b\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=readfile_mex
set MEX_NAME=readfile_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for readfile > readfile_mex.mki
echo COMPILER=%COMPILER%>> readfile_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> readfile_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> readfile_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> readfile_mex.mki
echo LINKER=%LINKER%>> readfile_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> readfile_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> readfile_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> readfile_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> readfile_mex.mki
echo OMPFLAGS= >> readfile_mex.mki
echo OMPLINKFLAGS= >> readfile_mex.mki
echo EMC_COMPILER=msvc150>> readfile_mex.mki
echo EMC_CONFIG=optim>> readfile_mex.mki
"C:\Program Files\MATLAB\R2018b\bin\win64\gmake" -j 1 -B -f readfile_mex.mk
