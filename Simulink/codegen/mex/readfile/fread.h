/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fread.h
 *
 * Code generation for function 'fread'
 *
 */

#ifndef FREAD_H
#define FREAD_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "readfile_types.h"

/* Function Declarations */
extern void b_fread(const emlrtStack *sp, real_T fileID, int32_T sizeA,
                    emxArray_real_T *A, real_T *count);

#endif

/* End of code generation (fread.h) */
