/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_readfile_api.c
 *
 * Code generation for function '_coder_readfile_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "readfile.h"
#include "_coder_readfile_api.h"
#include "readfile_emxutil.h"
#include "readfile_data.h"

/* Variable Definitions */
static emlrtRTEInfo d_emlrtRTEI = { 1, /* lineNo */
  1,                                   /* colNo */
  "_coder_readfile_api",               /* fName */
  ""                                   /* pName */
};

/* Function Declarations */
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_char_T *u);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *filename,
  const char_T *identifier, char_T y_data[], int32_T y_size[2]);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y_data[], int32_T y_size[2]);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret_data[], int32_T ret_size[2]);

/* Function Definitions */
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_char_T *u)
{
  const mxArray *y;
  const mxArray *m30;
  y = NULL;
  m30 = emlrtCreateCharArray(2, *(int32_T (*)[2])u->size);
  emlrtInitCharArrayR2013a(sp, u->size[1], m30, &u->data[0]);
  emlrtAssign(&y, m30);
  return y;
}

static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *filename,
  const char_T *identifier, char_T y_data[], int32_T y_size[2])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  h_emlrt_marshallIn(sp, emlrtAlias(filename), &thisId, y_data, y_size);
  emlrtDestroyArray(&filename);
}

static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y_data[], int32_T y_size[2])
{
  l_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y_data, y_size);
  emlrtDestroyArray(&u);
}

static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret_data[], int32_T ret_size[2])
{
  static const int32_T dims[2] = { 1, 1024 };

  const boolean_T bv1[2] = { false, true };

  int32_T iv16[2];
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "char", false, 2U, dims, &bv1[0],
    iv16);
  ret_size[0] = iv16[0];
  ret_size[1] = iv16[1];
  emlrtImportArrayR2015b(sp, src, (void *)ret_data, 1, false);
  emlrtDestroyArray(&src);
}

void readfile_api(const mxArray * const prhs[1], int32_T nlhs, const mxArray
                  *plhs[1])
{
  emxArray_char_T *y;
  char_T filename_data[1024];
  int32_T filename_size[2];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_char_T(&st, &y, 2, &d_emlrtRTEI, true);

  /* Marshall function inputs */
  g_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "filename", filename_data,
                     filename_size);

  /* Invoke the target function */
  readfile(&st, filename_data, filename_size, y);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, y);
  emxFree_char_T(&y);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

/* End of code generation (_coder_readfile_api.c) */
