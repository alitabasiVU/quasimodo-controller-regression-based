/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fclose.h
 *
 * Code generation for function 'fclose'
 *
 */

#ifndef FCLOSE_H
#define FCLOSE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "readfile_types.h"

/* Function Declarations */
extern void b_fclose(const emlrtStack *sp, real_T fileID);

#endif

/* End of code generation (fclose.h) */
