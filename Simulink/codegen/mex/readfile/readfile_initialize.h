/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * readfile_initialize.h
 *
 * Code generation for function 'readfile_initialize'
 *
 */

#ifndef READFILE_INITIALIZE_H
#define READFILE_INITIALIZE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "readfile_types.h"

/* Function Declarations */
extern void readfile_initialize(void);

#endif

/* End of code generation (readfile_initialize.h) */
