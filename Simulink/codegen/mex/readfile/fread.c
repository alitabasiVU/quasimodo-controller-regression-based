/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fread.c
 *
 * Code generation for function 'fread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "readfile.h"
#include "fread.h"
#include "readfile_emxutil.h"
#include "error.h"
#include "readfile_mexutil.h"

/* Variable Definitions */
static emlrtRSInfo n_emlrtRSI = { 50,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo o_emlrtRSI = { 66,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 80,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 163, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 113, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 424, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtMCInfo e_emlrtMCI = { 350, /* lineNo */
  1,                                   /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtMCInfo f_emlrtMCI = { 354, /* lineNo */
  9,                                   /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtDCInfo emlrtDCI = { 386,   /* lineNo */
  45,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRSInfo cb_emlrtRSI = { 350,/* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo db_emlrtRSI = { 354,/* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

/* Function Declarations */
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *t, const
  char_T *identifier, emxArray_real_T *y);
static void e_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, const mxArray *f,
                    emlrtMCInfo *location, const mxArray **g, const mxArray **h);
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static const mxArray *size(const emlrtStack *sp, const mxArray *b, const mxArray
  *c, emlrtMCInfo *location);

/* Function Definitions */
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *t, const
  char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  f_emlrt_marshallIn(sp, emlrtAlias(t), &thisId, y);
  emlrtDestroyArray(&t);
}

static void e_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, const mxArray *f,
                    emlrtMCInfo *location, const mxArray **g, const mxArray **h)
{
  const mxArray *pArrays[5];
  const mxArray *mv0[2];
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  pArrays[4] = f;
  emlrtAssign(g, emlrtCallMATLABR2012b(sp, 2, &mv0[0], 5, pArrays, "feval", true,
    location));
  emlrtAssign(h, mv0[1]);
}

static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  k_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[1] = { -1 };

  const boolean_T bv0[1] = { true };

  int32_T iv15[1];
  int32_T i2;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims, &bv0[0],
    iv15);
  i2 = ret->size[0];
  ret->size[0] = iv15[0];
  emxEnsureCapacity_real_T(sp, ret, i2, (emlrtRTEInfo *)NULL);
  emlrtImportArrayR2015b(sp, src, ret->data, 8, false);
  emlrtDestroyArray(&src);
}

static const mxArray *size(const emlrtStack *sp, const mxArray *b, const mxArray
  *c, emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  const mxArray *m37;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(sp, 1, &m37, 2, pArrays, "size", true, location);
}

void b_fread(const emlrtStack *sp, real_T fileID, int32_T sizeA, emxArray_real_T
             *A, real_T *count)
{
  const mxArray *y;
  const mxArray *m23;
  static const int32_T iv12[2] = { 1, 5 };

  static const char_T u[5] = { 'f', 'r', 'e', 'a', 'd' };

  const mxArray *b_y;
  const mxArray *m24;
  const mxArray *c_y;
  const mxArray *m25;
  const mxArray *d_y;
  const mxArray *m26;
  static const int32_T iv13[2] = { 1, 4 };

  static const char_T precision[4] = { 'c', 'h', 'a', 'r' };

  const mxArray *e_y;
  const mxArray *t = NULL;
  const mxArray *b_count = NULL;
  const mxArray *m27;
  real_T nrows;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &n_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  if ((fileID != 0.0) && (fileID != 1.0) && (fileID != 2.0)) {
  } else {
    b_st.site = &o_emlrtRSI;
    error(&b_st);
  }

  b_st.site = &p_emlrtRSI;
  c_st.site = &r_emlrtRSI;
  d_st.site = &s_emlrtRSI;
  c_st.site = &q_emlrtRSI;
  y = NULL;
  m23 = emlrtCreateCharArray(2, iv12);
  emlrtInitCharArrayR2013a(&c_st, 5, m23, &u[0]);
  emlrtAssign(&y, m23);
  b_y = NULL;
  m24 = emlrtCreateDoubleScalar(fileID);
  emlrtAssign(&b_y, m24);
  c_y = NULL;
  m25 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m25) = sizeA;
  emlrtAssign(&c_y, m25);
  d_y = NULL;
  m26 = emlrtCreateCharArray(2, iv13);
  emlrtInitCharArrayR2013a(&c_st, 4, m26, &precision[0]);
  emlrtAssign(&d_y, m26);
  e_y = NULL;
  m25 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m25) = 0;
  emlrtAssign(&e_y, m25);
  d_st.site = &cb_emlrtRSI;
  e_feval(&d_st, y, b_y, c_y, d_y, e_y, &e_emlrtMCI, &t, &b_count);
  d_st.site = &cb_emlrtRSI;
  *count = c_emlrt_marshallIn(&d_st, emlrtAlias(b_count), "count");
  y = NULL;
  m27 = emlrtCreateDoubleScalar(1.0);
  emlrtAssign(&y, m27);
  d_st.site = &db_emlrtRSI;
  nrows = c_emlrt_marshallIn(&d_st, size(&d_st, emlrtAlias(t), y, &f_emlrtMCI),
    "size");
  if (!(nrows >= 0.0)) {
    emlrtNonNegativeCheckR2012b(nrows, &emlrtDCI, &c_st);
  }

  if (nrows == 0.0) {
    A->size[0] = 0;
  } else {
    e_emlrt_marshallIn(&c_st, emlrtAlias(t), "t", A);
  }

  emlrtDestroyArray(&t);
  emlrtDestroyArray(&b_count);
}

/* End of code generation (fread.c) */
