/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fclose.c
 *
 * Code generation for function 'fclose'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "readfile.h"
#include "fclose.h"
#include "readfile_mexutil.h"

/* Variable Definitions */
static emlrtRSInfo t_emlrtRSI = { 15,  /* lineNo */
  "fclose",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\fclose.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 22,  /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtMCInfo g_emlrtMCI = { 186, /* lineNo */
  21,                                  /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static emlrtMCInfo h_emlrtMCI = { 186, /* lineNo */
  13,                                  /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static emlrtMCInfo i_emlrtMCI = { 187, /* lineNo */
  5,                                   /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static emlrtRSInfo v_emlrtRSI = { 186, /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtRSInfo x_emlrtRSI = { 187, /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2018b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

/* Function Declarations */
static boolean_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId);
static const mxArray *c_coder_internal_ifWhileCondExt(const emlrtStack *sp,
  const mxArray *b, emlrtMCInfo *location);
static boolean_T emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *d_coder_internal_ifWhileCondExt, const char_T *identifier);
static boolean_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static const mxArray *logical(const emlrtStack *sp, const mxArray *b,
  emlrtMCInfo *location);

/* Function Definitions */
static boolean_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId)
{
  boolean_T y;
  y = i_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *c_coder_internal_ifWhileCondExt(const emlrtStack *sp,
  const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  const mxArray *m33;
  pArray = b;
  return emlrtCallMATLABR2012b(sp, 1, &m33, 1, &pArray,
    "coder.internal.ifWhileCondExtrinsic", true, location);
}

static boolean_T emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *d_coder_internal_ifWhileCondExt, const char_T *identifier)
{
  boolean_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(d_coder_internal_ifWhileCondExt),
    &thisId);
  emlrtDestroyArray(&d_coder_internal_ifWhileCondExt);
  return y;
}

static boolean_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  boolean_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "logical", false, 0U, &dims);
  ret = *emlrtMxGetLogicals(src);
  emlrtDestroyArray(&src);
  return ret;
}

static const mxArray *logical(const emlrtStack *sp, const mxArray *b,
  emlrtMCInfo *location)
{
  const mxArray *pArray;
  const mxArray *m32;
  pArray = b;
  return emlrtCallMATLABR2012b(sp, 1, &m32, 1, &pArray, "logical", true,
    location);
}

void b_fclose(const emlrtStack *sp, real_T fileID)
{
  const mxArray *y;
  const mxArray *m28;
  static const int32_T iv14[2] = { 1, 6 };

  static const char_T u[6] = { 'f', 'c', 'l', 'o', 's', 'e' };

  const mxArray *b_y;
  const mxArray *m29;
  const mxArray *failp = NULL;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &t_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &u_emlrtRSI;
  y = NULL;
  m28 = emlrtCreateCharArray(2, iv14);
  emlrtInitCharArrayR2013a(&b_st, 6, m28, &u[0]);
  emlrtAssign(&y, m28);
  b_y = NULL;
  m29 = emlrtCreateDoubleScalar(fileID);
  emlrtAssign(&b_y, m29);
  c_st.site = &v_emlrtRSI;
  emlrtAssign(&failp, logical(&c_st, feval(&c_st, y, b_y, &g_emlrtMCI),
    &h_emlrtMCI));
  c_st.site = &x_emlrtRSI;
  emlrt_marshallIn(&c_st, c_coder_internal_ifWhileCondExt(&c_st, emlrtAlias
    (failp), &i_emlrtMCI), "coder.internal.ifWhileCondExtrinsic");
  emlrtDestroyArray(&failp);
}

/* End of code generation (fclose.c) */
