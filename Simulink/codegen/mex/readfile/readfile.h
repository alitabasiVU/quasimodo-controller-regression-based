/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * readfile.h
 *
 * Code generation for function 'readfile'
 *
 */

#ifndef READFILE_H
#define READFILE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "readfile_types.h"

/* Function Declarations */
extern void readfile(const emlrtStack *sp, const char_T filename_data[], const
                     int32_T filename_size[2], emxArray_char_T *y);

#endif

/* End of code generation (readfile.h) */
