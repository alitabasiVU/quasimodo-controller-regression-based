clear
close all
clc

% Expand path
addpath('src');
addpath('wr-actuator-library');
addpath('wr-actuator-library/ParameterFiles');
addpath('src/IMUCalibratioin/EulerLib')
addpath('src/IMUCalibratioin/QuaternionLib')

% addpath('RegModPrediction')
% `WE2_library_init` will add the remaining paths

% Call init script of library
WE2_library_init;

% Clear some things we don't need, to limit cluttering
clearvars WE_Backpack_params WE_BackpackBus BackpackBus JointParams ...
    WE_ForcePlateBus WE_IMUBus forceplate SilverTestActuator

% Load our own parameter file
load_quasimodo_params;
load_force_sensor;
load_imu_bus;
load_debug_bus;

%% High-level Controller Parameters

PassiveParams = load('src/PassiveParams.mat');
fid = fopen('PassiveParams.txt', 'w');
fprintf(fid, '%.4f\n', PassiveParams.PassiveMomentParams);
fclose(fid);

IMUCalibVars =  load('src/IMUCalibrationResults.mat');
IMUCalibVarsTXT = [IMUCalibVars.CalibVars.Static.Pelvis.QuatRot ...
                   IMUCalibVars.CalibVars.Static.T10.QuatRot ...
                   IMUCalibVars.CalibVars.Walk.Pelvis.Qpe...
                   IMUCalibVars.CalibVars.Walk.T10.Qt10	];
fid = fopen('IMUCalibVars.txt', 'w');
fprintf(fid, '%.4f\n', IMUCalibVarsTXT);
fclose(fid);

CalibVars.PassiveParams = PassiveParams;
CalibVars.IMUCalibVars = IMUCalibVars;
ControlParams = struct;

% Variable cannot be used in model, since that will replace the
% parameters with a bus
ControlParams.Mode = uint8(0);
ControlParams.MaxTorque = double(zeros(1, 4));
ControlParams.ROM_Min = double(zeros(1, 4));
ControlParams.ROM_Max = double(zeros(1, 4));
ControlParams.MaxRefVel = 0.0;
ControlParams.Kp = 0.0;
ControlParams.Kd = 0.0;


ControlParams.ThoraxLength = 0.0;
ControlParams.LumbarLength = 0.0;
ControlParams.BodyMass = 0.0;
%ControlParams.SupportRatio = 0.0;

%ControlParams.JointRatio = double(zeros(1, 4));

% New Variables---------------------------------
ControlParams.RatioDesActTorque = 0.0;
ControlParams.RatioLumbarHipTorque = 0.0; 
ControlParams.MaxInclinationAngleEstimation = 0;
ControlParams.MinInclinationAngleEstimation = 0;
ControlParams.MaxLumbarAngleEstimation = 0;
ControlParams.MinLumbarAngleEstimation = 0;
ControlParams.MaxIMUEncoderDeviation = 0;



%--------------------------------------------

ControlParamsBus = struct2bus(ControlParams, 'ControlParamsBus');


