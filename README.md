# Quasimodo Controller

This is the repository for the Quasimodo control. The high-level controller is the most important file. The [WR Actuator Library](https://bitbucket.org/ctw-bw/wr-actuator-library) is used for the low-level control.

## Build

 1. Init and update the `wr-actuator-library` submodule or use an external clone.
 2. Go to `Simulink/` and run `init_quasimodo.m`.
 3. Open `QuasimodoActuator.slx` and compile it.
 4. Open `QuasimodoController.slx` and compile it.

## Info

### Actuator Names

The actuators are named:

 * LLF: Left Lumbar Flexion
 * LHF: Left Hip Flexion
 * RLF: Right Lumbar Flexion
 * RHF: Right Hip Flexion

(From the perspective of the exo user.)
