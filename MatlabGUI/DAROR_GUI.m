function DAROR_GUI(l_ua)
addpath('functions\')
addpath('functions\rom\')
if (nargin == 0)
    l_ua = 0.335;
end
%% to avoid creating hundreds of timers
listOfTimers = timerfindall;
if ~isempty(listOfTimers)
    delete(listOfTimers(:));
end
%% Settings
step_horiz = 30;
step_elev = 30;
reverse_axis= 0;
if ~(mod(360,step_horiz)==0 && mod(180,step_elev)==0)
    error('Stepsizes do not in integer amount in complete [360,180] degree range')
end

%% ADS
asm = NET.addAssembly('C:\TwinCAT\AdsApi\.NET\v4.0.30319\TwinCAT.Ads.dll');
import TwinCAT.Ads.*
ads_netid_str = '127.0.0.1.1.1';
ads_port = 350;
tcClient = TwinCAT.Ads.TcAdsClient;
ams_id = TwinCAT.Ads.AmsNetId(ads_netid_str);
tcClient.Connect(ams_id, ads_port);

%% Read target
source_name = 'Object2 (cupid_arm_exo_demonstrator).Output.gui_out';
try
    sourceBlock = tcClient.CreateVariableHandle(source_name);
catch e
    error('I cannot find my signals in the ADS server. Possibly TwinCAT3 is not in run-mode, or the required model is not running.');
end
fprintf('[%s] ADS connection successful.\n',datestr(now,'HH:MM:SS'))
length_target = 60*8; %60 vars, 8 bytes per double = 480 bytes
dataStream_target = TwinCAT.Ads.AdsStream(length_target);
binRead_target = TwinCAT.Ads.AdsBinaryReader(dataStream_target);

%% Send targets
ads.target(1).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.EnableDrives1_Value';
ads.target(2).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.DisableDrives1_Value';
ads.target(3).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.enable_grav_comp_Value';
ads.target(4).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.enable_collision_avoidance_Valu'; %zonder e !
ads.target(5).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.RecalibrateMotorEncoders_Value';
ads.target(6).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.ResetForceTorqueSensor_Value';

ads.target(7).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.HoldMyCurrentPos_Value';
ads.target(8).name = 'Object2 (cupid_arm_exo_demonstrator).ModelParameters.AllowMeasurement_Value';

for idx = 1:length(ads.target)
    ads.target(idx).size = 1; ads.target(idx).datasize = ads.target(idx).size*8;
    
try
    ads.target(idx).handle = tcClient.CreateVariableHandle(ads.target(idx).name);
catch e
    fprintf('Problematic handle: %s\n',ads.target(idx).name)
    error(e.message);
end
end

%% GUI Window
hf = figure();
set(hf, 'MenuBar', 'none', 'ToolBar', 'none','CloseRequestFcn',@(obj,event) close_req_fcn(obj,event));
hf.Units = 'normalized';
set(hf,'outerposition',[0.05 0.1 0.9 0.85],'Name','DAROR-01 GUI','NumberTitle','off');
handles = initGUI();
handles.fighandle = hf;
pause(0.3)

%% GUI Timer
gui_dt = 0.1; %20 Hz update
looptimer = timer('StartDelay', 0, 'Period', gui_dt,'ExecutionMode','fixedRate');
looptimer.StartFcn = @timer_start_fcn;
looptimer.StopFcn = @timer_stop_fcn;
looptimer.TimerFcn = {@timer_update_fcn,handles};
running = 1;
motor_state_prev = 2;
current_force = zeros(3,1);
% while(running)
%     %
%     pause(1.0)
% end
%% Start
start(looptimer)

%% Callbacks

    function  close_req_fcn(hObject, eventdata)
        selection = questdlg('Close GUI?',...
            'Close Request',...
            'Yes','No','Yes');
        switch selection
            case 'Yes'
                running = 0;
                delete(get(0,'CurrentFigure'))
                
            case 'No'
                return
        end
    end

    function timer_start_fcn(obj, event)
        fprintf('[%s] GUI Started.\n',datestr(now,'HH:MM:SS'))
    end

    function timer_stop_fcn(obj, event)
        tcClient.Close;
%         clc
        fprintf('[%s] ADS connection closed.\n',datestr(now,'HH:MM:SS'))
    end

    function timer_update_fcn(obj, event, h)
        try
            tcClient.Read(sourceBlock,dataStream_target);
        catch e
            %disp(e) %debug
            error('Unable to read from ADS server. Was TwinCAT3 stopped?')
            %             return % debug
        end
        dataRead = zeros(length_target/8,1);
        for ind = 1:length_target/8
            dataRead(ind) = binRead_target.ReadDouble;
        end
        %         disp(dataRead'); %debug
        dataStream_target.Position = 0;
        q = dataRead(1:4);
        dq = dataRead(5:8);
        e = dataRead(9:12);
        de = dataRead(13:16);
        ISB_ang = dataRead(17:19);
        collision_code = dataRead(20);
        JointErrorLatched = dataRead(21:24);
        JointErrorCurrent = dataRead(25:28);
        DriveEnabled = dataRead(29:32);
        Wrench = dataRead(33:38);
        current_force = Wrench(1:3);
        
        JT_desired = dataRead(39:42);
        JT_est = dataRead(43:46);
        params_est = dataRead(47:58);
        grav_torque_model_error = dataRead(59);
        measurement_counter = dataRead(60);
        
        %         try %debug
        updateGUI(q,l_ua,h)%,h_R,h_3d,h_g)
        set(handles.gui_elements.txt_measpoints,'string',sprintf('#: %i, err: %g',measurement_counter,grav_torque_model_error));
        clear msg_string
        h.text.String = ' ';
        motors_enabled = ~any(~boolean(DriveEnabled));
        if (motor_state_prev ~= motors_enabled)
            if (motors_enabled)
                set(h.gui_elements.txt_motorstate,'string','Motors are: ON','ForegroundColor',[0 0.5 0]);
                handles.gui_elements.btn_enable.Enable = 'off';
                handles.gui_elements.btn_resetmotorenc.Enable = 'off';
                %handles.gui_elements.btn_disable.Enable = 'on';
            else
                set(h.gui_elements.txt_motorstate,'string','Motors are: OFF','ForegroundColor','red');
                handles.gui_elements.btn_enable.Enable = 'on';
                handles.gui_elements.btn_resetmotorenc.Enable = 'on';
                %handles.gui_elements.btn_disable.Enable = 'off';
            end
            motor_state_prev = motors_enabled;
        end
        
        
        msg_j_string = decodeJointError(JointErrorLatched);
        
        msg_c_string = char(decodeCollision(collision_code));
        
        h.text_joint.String = msg_j_string;
        h.text_coll.String = msg_c_string;
        %         catch e %debug
        %            stop(looptimer);
        %            error(e.message);
        %         end
    end

    function [handles] = initGUI()
        handles.subplot3d = subplot(2,2,[1 3]);
        handles.model_3d = plotDAROR([0;0;0;0],0.335,zeros(3,1),zeros(3,4)); view(90,0)
        axis([-0.5 0.5 -0.5 0.5 -0.75 0.25])
        rotate3d(handles.subplot3d,'on')
        handles.title_3d = title(' ');
        
        %map
        handles.subplotisb = subplot(2,2,2);
        if (reverse_axis)
            set(handles.subplotisb,'XDir','reverse')
        end
        
        %Calc number of elevation and horiz lines
        nElev = ceil(180/step_elev)+1;
        nHoriz = ceil((360-step_horiz)/step_horiz)+1;
        plotgrid_fcn,hold on
        handles.isb_cursor = plot(0,0,'r.','MarkerSize',30); %cursor
        sc = 3;%4;
        handles.isb_axial = quiver(nan,nan,nan,nan,sc*2);
        handles.isb_axial.ShowArrowHead = 'off';
        handles.isb_axial.Color = [1 0 0];
        handles.isb_axial.LineWidth = 2;
        hold off
        
        AxMin = nan(nElev,nHoriz);
        AxMax = nan(nElev,nHoriz);
        arcHandles = zeros(nElev,nHoriz);
        for i = 1:nElev %elev
            for j = 1:nHoriz %Azim -90 to 240
                arcHandles(i,j) = plotArc(0,0,AxMin(i,j),AxMax(i,j));
            end
        end
        
        handles.subplot_interface = subplot(2,2,4);
        text(0,-0.25,'Low-Level Error Code Latch:', 'VerticalAlignment','top', 'fontweight', 'bold');
        text(0.5,-0.25,'Mid-Level Collision/Workspace Code:', 'VerticalAlignment','top', 'fontweight', 'bold');
        handles.text_joint = text(0,-0.15,' ', 'VerticalAlignment','top');
        handles.text_coll = text(0.5,-0.15,' ', 'VerticalAlignment','top');
        set(handles.subplot_interface,'YDir','reverse')
        axis off
        
        %add gui elements
        handles.gui_panel = uipanel('Position',[0.525 0.02 0.45 0.15]); %x,y, w, h (from bottom)
        handles.gui_elements.chk_gravcomp =  uicontrol(handles.gui_panel,'style','checkbox','callback',{@cb_fun_chk_gravcomp},'units','normalized','position',[0.25,0.7,0.2,0.2],'string','Gravity Comp','min',0,'max',1);
        handles.gui_elements.btn_resetmotorenc =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_btn_reset_motenc},'units','normalized','position',[0.25,0.1,0.2,0.3],'string','Reset Mot. Enc.','min',0,'max',1);
        handles.gui_elements.chk_collision =  uicontrol(handles.gui_panel,'style','checkbox','callback',{@cb_fun_chk_collision},'units','normalized','position',[0.25,0.5,0.2,0.2],'string','Collsion Avoidance','min',0,'max',1);
        handles.gui_elements.btn_enable =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_fun_btn_enable},'units','normalized','position',[0.025,0.6,0.2,0.3],'string','Enable Motors');
        handles.gui_elements.btn_disable =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_fun_btn_disable},'units','normalized','position',[0.025,0.1,0.2,0.3],'string','Disable Motors');
        handles.gui_elements.sldr_gravcomp =  uicontrol(handles.gui_panel,'style','slider','callback',{@cb_fun_sldr_gravcomp},'units','normalized','position',[0.40,0.725,0.25,0.15],'string','Grav. Comp.','min',0,'max',2,'value',0,'enable','off','SliderStep',[0.05 0.10]);
        handles.gui_elements.btn_reset_fts =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_btn_reset_fts},'units','normalized','position',[0.525,0.1,0.15,0.3],'string','Reset F/T Sensor');
        handles.gui_elements.txt_motorstate =  uicontrol(handles.gui_panel,'style','text','units','normalized','position',[0.025,0.45,0.2,0.1],'string','Motors are: ???','FontWeight','bold');
        
        handles.gui_elements.btn_hold_pos =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_btn_hold_pos},'units','normalized','position',[0.675,0.1,0.15,0.3],'string','Hold Pos.');
        handles.gui_elements.btn_rel_pos =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_btn_rel_pos},'units','normalized','position',[0.825,0.1,0.15,0.3],'string','Release Pos.');
        handles.gui_elements.btn_take_meas =  uicontrol(handles.gui_panel,'style','pushbutton','callback',{@cb_btn_take_meas},'units','normalized','position',[0.825,0.6,0.15,0.3],'string','Take Grav. Meas.');
        handles.gui_elements.txt_measpoints =  uicontrol(handles.gui_panel,'style','text','units','normalized','position',[0.80,0.5,0.2,0.1],'string','#','FontWeight','bold');

        drawnow
    end

    function cb_fun_chk_gravcomp(obj,event)
        if (obj.Value)
            handles.gui_elements.sldr_gravcomp.Enable = 'on';
            val = handles.gui_elements.sldr_gravcomp.Value;
            sendADSMessage(ads.target(3),val);
        else
            handles.gui_elements.sldr_gravcomp.Enable = 'off';
            sendADSMessage(ads.target(3),0.0);
            handles.gui_elements.sldr_gravcomp.Value = 0.0;
        end
    end
    function cb_fun_chk_collision(obj,event)
       value = double(handles.gui_elements.chk_collision.Value);
       sendADSMessage(ads.target(4),value);
    end
    function cb_fun_btn_enable(obj,event)
       sendADSMessage(ads.target(1),1);
       sendADSMessage(ads.target(2),0);
    end
    function cb_fun_btn_disable(obj,event)
       sendADSMessage(ads.target(1),0);
       sendADSMessage(ads.target(2),1);
    end
    function cb_fun_sldr_gravcomp(obj,event)
       val = double(handles.gui_elements.sldr_gravcomp.Value);
       sendADSMessage(ads.target(3),val);
    end
    function cb_btn_reset_fts(obj,event)
       sendADSMessage(ads.target(6),1);
       pause(0.05)
       sendADSMessage(ads.target(6),0);
    end
    function cb_btn_reset_motenc(obj,event)
       sendADSMessage(ads.target(5),1);
       pause(0.25)
       sendADSMessage(ads.target(5),0);
    end
    function cb_btn_hold_pos(obj,event)
        sendADSMessage(ads.target(7),1);
        handles.gui_elements.btn_hold_pos.Enable = 'off';
        handles.gui_elements.btn_rel_pos.Enable = 'on';
    end
    function cb_btn_rel_pos(obj,event)
        sendADSMessage(ads.target(7),0);
        handles.gui_elements.btn_hold_pos.Enable = 'on';
        handles.gui_elements.btn_rel_pos.Enable = 'off';
    end
    function cb_btn_take_meas(obj,event)
        sendADSMessage(ads.target(8),1);
        pause(0.5);
        sendADSMessage(ads.target(8),0);
    end

    function sendADSMessage(target,value)
        %send only 1 double
        data_bytes = typecast(double(value(1)), 'uint8');
        try
            tcClient.WriteAny(target.handle,data_bytes);
        catch e
            error(e.message);
        end
    end

    function updateGUI(q,l_ua,h)%,h_R,h_3d,h_g)
        [~,~,~,~,~,H3_0_r,~,~,~,~,~,~,~,~,~] = forwardKinematics([q(1);-q(2);q(3);q(4)],l_ua);
        R_in_0 = H3_0_r(1:3,1:3);
        R_in_ISB = Ry(90/180*pi)*R_in_0;
        
        [elevationRotation,horizontalRotation,axialRotation] = getISBAngles(R_in_ISB);
        
        
        [horizontalRotationT,elevationRotationT] = rec2moll([horizontalRotation,elevationRotation]);
        r = 3;
        v = [cosd(axialRotation);-sind(axialRotation)]*r;
        
        updatePlot(h.model_3d,q,l_ua,current_force,zeros(3,4));
        set(h.title_3d,'string',sprintf('GH Elevation: %0.1f, GH Horizontal Rot: %0.1f, Axial Rotation: %0.1f [deg]\nJoint angles:%0.1f, %0.1f, %0.1f, %0.1f [deg]',elevationRotation,horizontalRotation,axialRotation,q(1)/pi*180,q(2)/pi*180,q(3)/pi*180,q(4)/pi*180));
        
        set(h.isb_cursor,'XData',horizontalRotationT,'YData',elevationRotationT);
        set(h.isb_axial,'XData',horizontalRotationT,'YData',elevationRotationT,'UData',v(1),'VData',v(2));
        drawnow
    end
    function R = Rx(q)
        R = [1 0 0; 0 cos(q) -sin(q); 0 sin(q) cos(q)];
    end
    function R = Ry(q)
        R = [cos(q) 0 sin(q);0 1 0; -sin(q) 0 cos(q)];
    end
    function R = Rz(q)
        R = [cos(q) -sin(q) 0; sin(q) cos(q) 0; 0 0 1];
    end
    function hnd = plotH(H,lw,a,scale)
        %correct visualization with R
        R = [0 0 1; 1 0 0; 0 1 0];
        p = R*H(1:3,4);
        RR = R*H(1:3,1:3);
        c = eye(3);
        for n = 3:-1:1 %better mem alloc
            hnd(n) = plot3(p(1)+scale*[0 RR(1,n)],p(2)+scale*[0 RR(2,n)],p(3)+scale*[0 RR(3,n)],'linewidth',lw,'color',[c(n,:) a]);
        end
    end
    function plotgrid_fcn()
        dm = 1;
        set(gca,'ytick',[]) ;
        set(gca,'xtick',[]) ;
        set(gca,'XColor',[1 1 1],'YColor',[1 1 1])
        hold on
        
        %% plot figurine
        t = linspace(0,2*pi,720);
        %linker-arm
        x = ((120 + 5*cos(t))-90)*3+90;
        y = ((90 + 5*sin(t))-90)*3+90;
        p = patch(x,y,'red','EdgeColor','none');
        set(p,'FaceColor',[0.825 0.825 0.825]);
        x = ((120 + 4*cos(t))-90)*3+90;
        y = ((60 + 4*sin(t))-90)*3+90;
        p = patch(x,y,'red','EdgeColor','none');
        set(p,'FaceColor',[0.825 0.825 0.825]);
        x = ([115 125 124 116]-90)*3+90;
        y = ([90 90 60 60]-90)*3+90;
        p = patch(x,y,'red','EdgeColor','none');
        set(p,'FaceColor',[0.825 0.825 0.825]);
        
        %lichaam
        x = ([97.5 112.5 120 90]-90)*3+90;
        y = ([60 60 95 95]-90)*3+90;
        p = patch(x,y,'red','EdgeColor','none');
        set(p,'FaceColor',[0.825 0.825 0.825]);
        
        %hoofd
        x = ((105 + 10*cos(t))-90)*3+90;
        y = ((107.5 + 10*sin(t))-90)*3+90;
        p = patch(x,y,'red','EdgeColor','none');
        set(p,'FaceColor',[0.825 0.825 0.825]);
        
        %arm
        if(~reverse_axis)
            x = 90 + 15*cos(t);
            y = 90 + 15*sin(t);
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.675 0.675 0.675]);
            x = 160 + 12*cos(t);
            y = 90 + 12*sin(t);
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.675 0.675 0.675]);
            x = [90 90 160 160];
            y = [75 105 102 78];
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.675 0.675 0.675]);
        else
            x = 90 + 15*cos(t);
            y = 90 + 15*sin(t);
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.825 0.825 0.825]);
            x = 160 + 12*cos(t);
            y = 90 + 12*sin(t);
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.825 0.825 0.825]);
            x = [90 90 160 160];
            y = [75 105 102 78];
            p = patch(x,y,'red','EdgeColor','none');
            set(p,'FaceColor',[0.825 0.825 0.825]);
        end
        
        %% grid
        for az = [-90:step_horiz:270]
            el = [0:dm:180];
            x_m = zeros(length(el),1);
            y_m = zeros(length(el),1);
            for n = 1:length(el)
                [P_out(1),P_out(2)] = rec2moll([az;el(n)]);
                x_m(n) = P_out(1);
                y_m(n) = P_out(2);
            end
            if (az == 270)
                plot(x_m,y_m,'k--');
            else
                plot(x_m,y_m,'k')
            end
        end
        for el = [-0:step_elev:180]
            az = [-90:5:270];
            x_m = zeros(length(az),1);
            y_m = zeros(length(az),1);
            for n = 1:length(az)
                [P_out(1),P_out(2)] = rec2moll([az(n);el]);
                x_m(n) = P_out(1);
                y_m(n) = P_out(2);
            end
            plot(x_m,y_m,'k');
        end
        for x = [-90:step_horiz:270]
            text(x+5,77.5,num2str(x),'HorizontalAlignment','left')
        end
        az = -90;
        el = [0:step_elev:180];
        x_m = zeros(length(el),1);
        y_m = zeros(length(el),1);
        for n = 2:length(el)-1
            [P_out(1),P_out(2)] = rec2moll([az;el(n)]);
            x_m(n) = 1.125*P_out(1);
            y_m(n) = 90+1.2*(P_out(2)-90);
            text(x_m(n),y_m(n),num2str(el(n)),'HorizontalAlignment','right');
        end
        text(90,-15,'0','HorizontalAlignment','center')
        text(90,195,'180','HorizontalAlignment','center')
        
        text(-120,90,'GH Elevation Rotation [^\circ]','rotation',90,'HorizontalAlignment','center')
        text(90,-25,'GH Horizontal Rotation [^\circ]','rotation',0,'HorizontalAlignment','center')
        th = title('ISB-definition of shoulder pose');
        
        titlepos = get(th,'Position');
        set(th,'FontSize',8,'Position',titlepos + [0 17.5 0])
        h_o = plot(nan,nan,'k.','LineWidth',2,'MarkerSize',5);
        axis equal
        axis([-100 280 -10 190])
        hold off
        
    end
end